#!/bin/sh
#SBATCH -n 1
#SBATCH -p gpu_short
#SBATCH -t 0:10:00
#SBATCH -o out/test_load_%A.o
#SBATCH -e out/test_Load_%A.e

module load python/3.5.2
module load cuda/8.0.44
module load cudnn/8.0-v6.0


srun -u python3 test_load.py

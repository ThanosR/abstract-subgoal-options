import socket
runs_locally = socket.gethostname() == "thanos-Alienware-15-R3"


import numpy as np
import torch
import gym
import argparse
import os
import time

import utils
import TD3
import pickle
import ActionSpaces

from tensorboardX import SummaryWriter

from ReplayBuffer import ReplayBuffer, ReplayBufferHigh
import FixedPolicies

from utils import EnvWithGoal
from utils import success_fn
from utils import HistoryRewardNormalizer

np.set_printoptions(precision=4)
np.set_printoptions(suppress=True)
np.set_printoptions(linewidth=130)

# Runs policy for X episodes and returns average reward
def evaluate_policy(policy_low, policy_high, eval_episodes=10, c=10, state_to_goal_filter = None):
	avg_reward = 0.
	avg_intrinsic_reward = 0
	successes = 0

	state_action_history = []
	for ep in range(eval_episodes):
		episode_buffer = []

		obs = env.reset(True) if args.env_name.startswith('Ant') else env.reset()
		goal = policy_high.select_action(np.asarray(obs))

		done = False
		episode_timesteps = 0
		while not done:
			action = policy_low.select_action(np.append(np.array(obs), goal))

			new_obs, reward, done, _ = env.step(action if max_action is not None else np.argmax(action))

			r_i = utils.intrinsic_reward(obs, goal, action, new_obs, state_to_goal_filter, env.observation_space)

			if (episode_timesteps + 1) % c == 0:
				new_goal = policy_high.select_action(np.asarray(new_obs))

				if args.env_name == 'MyDiscretePendulum-v0':
					env.env.set_goal(new_obs + new_goal)
				if args.env_name == 'DoomGridworldGather-v0':
					env.env.set_goal(new_obs[:2] + new_goal)
			else:
				new_goal = utils.h(obs, goal, new_obs,state_to_goal_filter)

			episode_buffer.append([obs, goal, action, reward, r_i, done])

			goal = new_goal
			obs = new_obs

			avg_reward += reward
			avg_intrinsic_reward += r_i
			episode_timesteps += 1

			if(episode_timesteps == 500):
				done = True

			if(done and episode_timesteps < 500):
				successes += 1

			if runs_locally and  ep == 0:
				env.render()

		state_action_history.append(episode_buffer)

	avg_reward /= eval_episodes
	avg_intrinsic_reward /= eval_episodes
	avg_success = successes / eval_episodes

	print("---------------------------------------")
	print("Evaluation over %d episodes: %f  %f  %f" % (eval_episodes, avg_reward, avg_intrinsic_reward, avg_success))
	print("---------------------------------------")
	return avg_reward, avg_intrinsic_reward, avg_success, state_action_history





if __name__ == "__main__":

	#region args
	parser = argparse.ArgumentParser()

	parser.add_argument("--env_name", default="GridworldCollect-v0")  # OpenAI gym environment name
	parser.add_argument("--seed", default=0, type=int)  # Sets Gym, PyTorch and Numpy seeds
	parser.add_argument("--start_timesteps", default=1e4,type=int)  # How many time steps purely random policy is run for
	parser.add_argument("--eval_freq", default=4e3, type=float)  # How often (time steps) we evaluate
	parser.add_argument("--max_timesteps", default=2e6, type=float)  # Max time steps to run environment for
	parser.add_argument('--episode_length', default=500, type=int)
	parser.add_argument('--num_episodes', default=50, type=int)
	parser.add_argument("--save_models", action="store_false")  # Whether or not models are saved
	parser.add_argument("--run_directory", default="")  # Whether or not models are saved

	parser.add_argument("--batch_size", default=100, type=int)  # Batch size for both actor and critic
	parser.add_argument("--tau", default=0.005, type=float)  # Target network update rate
	parser.add_argument("--noise_clip", default=0.5, type=float)  # Range to clip target policy noise
	parser.add_argument("--policy_freq", default=2, type=int)  # Frequency of delayed policy updates
	parser.add_argument("--c", default=10, type=int)  # Frequency of delayed policy updates

	parser.add_argument("--filename_extension", default="")

	# Lower policy parameters
	parser.add_argument("--expl_noise", default=0.1, type=float)  # Std of Gaussian exploration noise
	parser.add_argument("--discount", default=0.99, type=float)  # Discount factor
	parser.add_argument("--policy_noise", default=0.2, type=float)  # Noise added to target policy during critic update
	parser.add_argument("--lr_actor", default=0.001, type=float)  # Std of Gaussian exploration noise
	parser.add_argument("--lr_critic", default=0.001, type=float)  # Std of Gaussian exploration noise

	# Higher policy parameters
	parser.add_argument("--expl_noise_h", default=0.2, type=float)  # Std of Gaussian exploration noise
	parser.add_argument("--discount_h", default=0.99, type=float)  # Discount factor
	parser.add_argument("--policy_noise_h", default=0.2, type=float)  # Noise added to target policy during critic update
	parser.add_argument("--lr_actor_h", default=0.001, type=float)  # Std of Gaussian exploration noise
	parser.add_argument("--lr_critic_h", default=0.001, type=float)  # Std of Gaussian exploration noise
	parser.add_argument("--no_goal_resamples", default=8, type=int)  # Batch size for both actor and critic

	args = parser.parse_args()

	#endregion

	# args.env_name = 'DoomGridworldGather-v0'
	args.save_models = True
	args.start_timesteps = 0

	#region Setup folders

	file_name = "HIRO_%s_%s" % (args.env_name, time.strftime("%Y%m%d-%H%M") )
	if args.filename_extension: file_name += "_" + args.filename_extension
	file_name_low = file_name + "_low"
	file_name_high = file_name + "_high"

	summary_dir = "./results/{}/{}/{}".format(args.env_name, args.run_directory, file_name)
	os.makedirs(summary_dir)

	model_dir = "{}/model".format(summary_dir)
	os.makedirs(model_dir)

	writer = SummaryWriter(summary_dir)

	print("---------------------------------------")
	print("Settings: %s" % (file_name))
	print("---------------------------------------")


	with open('{}/args.txt'.format(summary_dir), 'w') as output:
		for k in sorted(args.__dict__.keys()):
			output.writelines("{}: {}\n".format(k, args.__dict__[k]))
			print("{}: {}".format(k, args.__dict__[k]))

	#endregion

	env, goal_space, state_to_goal_filter, normalizer_high, normalizer_low = utils.setup_env(args)

	max_goal = goal_space.high
	observation_dim = env.observation_space.shape[0]
	goal_dim = goal_space.shape[0]

	if type(env.action_space) is gym.spaces.Discrete:
		action_dim = env.action_space.n
		max_action = None
	elif type(env.action_space) is gym.spaces.Box:
		action_dim = env.action_space.shape[0]
		max_action = env.action_space.high


	# Set seeds
	torch.manual_seed(args.seed)
	np.random.seed(args.seed)

	c = args.c

	actionSpace_High = ActionSpaces.ContinuousActionSpace(max_goal,args.noise_clip,args.policy_noise,args.expl_noise_h)
	actionSpace_Low = ActionSpaces.DiscreteActionSpace(action_dim, args.noise_clip, args.policy_noise,
	                                                  args.expl_noise)

	if args.filename_extension.startswith("low_fixed"):
		policy_high = TD3.TD3_2(observation_dim, actionSpace_High, lr_a=args.lr_actor_h, lr_c=args.lr_critic_h)
		policy_low = FixedPolicies.GridworldGatherPolicyLow(action_dim, env.env)
		args.expl_noise = 0
		replay_buffer_high = ReplayBufferHigh(policy_low=policy_low, goal_space=goal_space,
		                                      state_to_goal_filter=state_to_goal_filter, resamples_goal=False,
		                                      no_goal_samples=8)
		print("Running Low fixed (pathfinding), learning only High policy")

	elif args.filename_extension.startswith("high_fixed"):
		# Initialize policy
		policy_low = TD3.TD3(observation_dim + goal_dim, action_dim, max_action, lr_a=args.lr_actor, lr_c=args.lr_critic)
		policy_high = FixedPolicies.GridworldGatherPolicyHigh(env.env)
		args.expl_noise_h = 0
		replay_buffer_high = ReplayBufferHigh(policy_low=policy_low, goal_space=goal_space,
		                                      state_to_goal_filter=state_to_goal_filter, resamples_goal=False,
		                                      no_goal_samples=8)
		print("Running high fixed (closest pickup), learning only Low policy")

	else:
		policy_low = TD3.TD3_2(observation_dim + goal_dim, actionSpace_Low, lr_a=args.lr_actor, lr_c=args.lr_critic)
		policy_high = TD3.TD3_2(observation_dim, actionSpace_High, lr_a=args.lr_actor_h, lr_c=args.lr_critic_h)
		replay_buffer_high = ReplayBufferHigh(policy_low=policy_low, goal_space=goal_space,
		                                      state_to_goal_filter=state_to_goal_filter, resamples_goal=True,
		                                      no_goal_samples=args.no_goal_resamples)
		print("learning both policies, Low and High")

	replay_buffer_low = ReplayBuffer()


	# Evaluate untrained policy
	eval_result = evaluate_policy(policy_low, policy_high, c = args.c, state_to_goal_filter=state_to_goal_filter)
	evaluations = [eval_result[:-1]]
	eval_history = [eval_result[-1]]

	total_timesteps = 0
	timesteps_since_eval = 0
	episode_num = 0
	done = True

	high_action_buffer = []

	state_action_history = []
	episode_buffer = []


	while total_timesteps < args.max_timesteps:

		if done:
			if total_timesteps != 0:
				print(("Total T: %d Episode Num: %d Episode T: %d Reward: %f Reward_i: %f") % (
					total_timesteps, episode_num, episode_timesteps, episode_reward, episode_intrinsic_reward))

				writer.add_scalar('train/rewards', episode_reward, total_timesteps)
				writer.add_scalar('train/intrinsic_rewards', episode_intrinsic_reward, total_timesteps)

				loss_Q_low, loss_a_low = policy_low.train(replay_buffer_low, episode_timesteps, args.batch_size, args.discount, args.tau,
								 args.policy_noise, args.noise_clip, args.policy_freq)

				loss_Q_high, loss_a_high = policy_high.train(replay_buffer_high, episode_timesteps, args.batch_size // c,
									  args.discount, args.tau, args.policy_noise, args.noise_clip, args.policy_freq)

				writer.add_scalar('train/Q_loss_low', loss_Q_low, total_timesteps)
				writer.add_scalar('train/actor_loss_low', loss_a_low, total_timesteps)

				writer.add_scalar('train/Q_loss_high', loss_Q_high, total_timesteps)
				writer.add_scalar('train/actor_loss_high', loss_a_high, total_timesteps)

				state_action_history.append(episode_buffer)
				episode_buffer = []

			# Evaluate episode
			if timesteps_since_eval >= args.eval_freq:
				timesteps_since_eval %= args.eval_freq
				eval_result = evaluate_policy(policy_low, policy_high, c = args.c, state_to_goal_filter=state_to_goal_filter)
				evaluations.append(eval_result[:-1])
				eval_history.append(eval_result[-1])

				if args.save_models:
					policy_low.save("low", directory=model_dir)
					policy_high.save("high", directory=model_dir)
					with open("{}/history.pkl".format(summary_dir, file_name), 'wb+') as history_file:
						pickle.dump(state_action_history, history_file)
					with open("{}/eval_history.pkl".format(summary_dir), 'wb+') as history_file:
						pickle.dump(eval_history, history_file)

				writer.add_scalar('eval/rewards', evaluations[-1][0], total_timesteps)
				writer.add_scalar('eval/intrinsic_rewards', evaluations[-1][1], total_timesteps)
				writer.add_scalar('eval/success', evaluations[-1][2], total_timesteps)

			if len(high_action_buffer) > 0:
				replay_buffer_high.add_high_exp(high_action_buffer, new_obs, done_bool)


			# Reset environment
			obs = env.reset(False) if args.env_name.startswith('Ant') else env.reset()
			goal = policy_high.select_action(np.asarray(obs))
			if args.expl_noise_h != 0:
				goal = goal + np.random.normal(0, args.expl_noise_h * max_goal, size=(1, goal_space.shape[0]))
				goal = goal.clip(goal_space.low, goal_space.high)
				goal = goal[0]


			high_action_buffer = []

			done = False
			episode_reward = 0
			episode_intrinsic_reward = 0
			episode_timesteps = 0
			episode_num += 1




		# Select action randomly or according to policy
		if total_timesteps < args.start_timesteps:
			action = env.action_space.sample()
			if max_action is None:
				action_np = np.zeros(action_dim)
				action_np[action] = 1
				action = action_np
		else:
			action = policy_low.select_action(np.append(np.array(obs), goal))
			# Add noise
			if max_action is not None:
				if args.expl_noise != 0:
					noise = np.random.normal(0, args.expl_noise, size=env.action_space.shape[0])
					action = action + max_action * noise
					action = action.clip(env.action_space.low, env.action_space.high)
			else:
				if np.random.rand() < args.expl_noise:
					action = np.zeros(action_dim)
					action[np.random.randint(0,action_dim)] = 1

		# Perform action
		new_obs, R, done, _ = env.step(action if max_action is not None else np.argmax(action))


		done_bool = 0 if episode_timesteps + 1 == env._max_episode_steps else float(done)
		r_i = utils.intrinsic_reward(obs, goal, action, new_obs, state_to_goal_filter, env.observation_space)
		R = normalizer_high.normalize(R)
		r_i = normalizer_low.normalize(r_i)


		high_action_buffer.append((obs, goal, action, R))

		# Store experience and select new goal
		if (episode_timesteps + 1) % c == 0:

			replay_buffer_high.add_high_exp(high_action_buffer, new_obs, done_bool)
			high_action_buffer = []

			if total_timesteps < args.start_timesteps:
				new_goal = goal_space.sample()
			else:
				new_goal = policy_high.select_action(np.asarray(new_obs))
				if args.expl_noise_h != 0:
					noise = np.random.normal(0, args.expl_noise_h * max_goal,size=(1, goal_space.shape[0]))
					new_goal = new_goal + noise
					new_goal = new_goal.clip(goal_space.low, goal_space.high)
					new_goal = new_goal[0]

			if args.env_name == 'MyDiscretePendulum-v0':
				env.env.set_goal(new_obs + new_goal)
			if args.env_name == 'DoomGridworldGather-v0':
				env.env.set_goal(new_obs[:2] + new_goal)


			filtered_obs_bounds = state_to_goal_filter(env.observation_space.nvec)
			filtered_obs = state_to_goal_filter(new_obs)
			goal = np.clip(goal, -filtered_obs_bounds - filtered_obs, filtered_obs_bounds - filtered_obs)

			done_bool = 1
		else:
			new_goal = utils.h(obs, goal, new_obs,state_to_goal_filter)


		# Store data in replay buffer
		replay_buffer_low.add((np.append(obs, goal), np.append(new_obs, new_goal), action, r_i, done_bool))

		episode_buffer.append([obs, goal, action, R, r_i, done])
		obs, goal = new_obs, new_goal
		episode_reward += R
		episode_intrinsic_reward += r_i
		episode_timesteps += 1
		total_timesteps += 1
		timesteps_since_eval += 1

		# print(obs, goal)
		# print(policy_low.actor(
		# 	torch.FloatTensor(np.append(np.array(obs), goal).reshape(1, -1)).to(TD3.device)).cpu().data.numpy().flatten())

	# print(obs, action)

		# env.render()

	# Final evaluation
	eval_result = evaluate_policy(policy_low, policy_high,c=args.c, state_to_goal_filter=state_to_goal_filter)
	evaluations.append(eval_result[:-1])
	eval_history.append(eval_result[-1])
	if args.save_models:
		policy_low.save("low", directory=model_dir)
		policy_high.save("high", directory=model_dir)
		with open("{}/history.pkl".format(summary_dir), 'wb+') as history_file:
			pickle.dump(state_action_history, history_file)
		with open("{}/eval_history.pkl".format(summary_dir), 'wb+') as history_file:
			pickle.dump(eval_history, history_file)

	# np.save("./results/%s" % (file_name), evaluations)

	writer.add_scalar('eval/rewards', evaluations[-1][0], total_timesteps)
	writer.add_scalar('eval/intrinsic_rewards', evaluations[-1][1], total_timesteps)
	writer.add_scalar('eval/success', evaluations[-1][2], total_timesteps)

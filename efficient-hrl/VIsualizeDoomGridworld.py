import gym
import environments
import pickle
import time
import numpy as np
from gym import wrappers
import os
import shutil
import utils
import TD3
from argparse import Namespace
import FixedPolicies

def RepresentsInt(s):
	try:
		int(s)
		return True
	except ValueError:
		return False

def RepresentsFloat(s):
	try:
		float(s)
		return True
	except ValueError:
		return False


def read_args(args_path):
	args = {}
	with open(args_path, "r") as args_txt:
		arguments = args_txt.readlines()
		for line in arguments:
			tokens = line.split()
			key = tokens[0][:-1]
			value = tokens[1]
			if RepresentsInt(value):
				value = int(value)
			elif RepresentsFloat(value):
				value = float(value)

			args[key] = value


		args = Namespace(**args)

		for k in sorted(args.__dict__.keys()):
			print("{}: {}".format(k, args.__dict__[k]))
	return args



show_history = True

step_dur = 0.05

if show_history:

	evaluate = True

	ep_id = 58

	source_dir = "/media/thanos/DATA/Projects/Python Projects/Deep Reinforcement Learning/abstract-subgoal-options/efficient-hrl/results/GridworldCollect-v0/Train_both/HIRO_GridworldCollect-v0_20181029-2241"
	video_directory = "{}/records/{}".format(source_dir, ep_id)

	if evaluate:
		with open("{}/eval_history.pkl".format(source_dir), "rb") as history_file:
			history = pickle.load(history_file)
		episode = history[ep_id - 1][0]

	else:
		with open("{}/history.pkl".format(source_dir), "rb") as history_file:
			history = pickle.load(history_file)
		episode = history[ep_id - 1]


	env = gym.make('GridworldCollect-v0')
	env = wrappers.Monitor(env, video_directory, video_callable=lambda episode_id: True,
	                       force=True)
	env.env.env.player.render_history = True


	rewards = []


	s = env.reset()
	env.render()
	last_time = time.time()
	while time.time() - last_time < step_dur:
		pass

	print('------------------------------------------')

	total_reward = 0
	total_reward_intrinsic = 0

	last_time = time.time()
	for step in range(len(episode)): # run for 1000 steps

		state_recorded = episode[step][0]
		goal_recorded = episode[step][1]
		action_recorded = episode[step][2]
		reward_recorded = episode[step][3]
		r_i_recorded = episode[step][4]
		done_recorded = episode[step][5]
		env.env.env.set_goal(s[:2] + goal_recorded.astype(int))

		# print(state_recorded, np.argmax(action_recorded))
		# print(s)
		# print("----")

		s, R, t, _ = env.step(np.argmax(action_recorded)) # take action
		total_reward += R
		total_reward_intrinsic += r_i_recorded
		env.render()

		last_time = time.time()
		while time.time() - last_time < step_dur:
			pass


	env.env.close()
	env.close()

	print(total_reward)
	print(total_reward_intrinsic)

	for file in os.listdir(video_directory):
		if(file.endswith(".mp4")):
			if evaluate:
				video_path = "{}/records/eval={}__R={}__r={}.mp4".format(source_dir, ep_id, total_reward, int(total_reward_intrinsic))
			else:
				video_path = "{}/records/EP={}__R={}__r={}.mp4".format(source_dir, ep_id, total_reward,
				                                                       int(total_reward_intrinsic))
			os.rename("{}/{}".format(video_directory, file), video_path)
			print("Saved as {0}".format(video_path))


	shutil.rmtree(video_directory)


else:

	source_dir = "/home/thanos/Desktop/L1_int_long_best/HIRO_DoomGridworldGather-v0_20181021-1709_low_fixed"

	args = read_args("{}/args.txt".format(source_dir))
	model_weights_directory = "{}/models".format(source_dir)


	video_directory = "{}/records/evaluation".format(source_dir)

	env, goal_space, state_to_goal_filter, normalizer_high, normalizer_low = utils.setup_env(args)
	env = wrappers.Monitor(env, video_directory, video_callable=lambda episode_id: True,force=True)
	env.env.env.player.render_history = True


	observation_dim = env.observation_space.shape[0]
	goal_dim = goal_space.shape[0]
	max_goal = goal_space.high
	action_dim = env.action_space.n
	max_action = None
	c = args.c

	if "HIRO" in source_dir:

		if "low_fixed" in source_dir:
			policy_high = TD3.TD3(observation_dim, goal_dim, max_goal, lr_a=args.lr_actor_h, lr_c=args.lr_critic_h)
			policy_low = FixedPolicies.GridworldGatherPolicyLow(action_dim, env.env.env)
			args.expl_noise = 0
			print("Running Low fixed (pathfinding), learning only High policy")

			policy_high.load("high", model_weights_directory)

		elif "high_fixed" in source_dir:
			policy_low = TD3.TD3(observation_dim + goal_dim, action_dim, max_action, lr_a=args.lr_actor, lr_c=args.lr_critic)
			policy_high = FixedPolicies.GridworldGatherPolicyHigh(env.env.env)
			args.expl_noise_h = 0
			print("Running high fixed (closest pickup), learning only Low policy")

		else:
			policy_low = TD3.TD3(observation_dim + goal_dim, action_dim, max_action, lr_a=args.lr_actor,lr_c=args.lr_critic)
			policy_high = TD3.TD3(observation_dim, goal_dim, max_goal, lr_a=args.lr_actor_h, lr_c=args.lr_critic_h)
			print("learning both policies, Low and High")

	elif "TD3" in source_dir:
		pass


	obs = env.reset()
	goal = policy_high.select_action(np.asarray(obs))
	env.render()
	last_time = time.time()
	while time.time() - last_time < step_dur:
		pass

	done = False
	episode_timesteps = 0

	total_reward = 0
	total_reward_intrinsic = 0

	while not done:
		action = policy_low.select_action(np.append(np.array(obs), goal))

		new_obs, reward, done, _ = env.step(action if max_action is not None else np.argmax(action))

		r_i = utils.intrinsic_reward(obs, goal, action, new_obs, state_to_goal_filter, env.observation_space)

		if (episode_timesteps + 1) % c == 0:
			new_goal = policy_high.select_action(np.asarray(new_obs))
			env.env.env.set_goal(new_obs[:2] + new_goal)
		else:
			new_goal = utils.h(obs, goal, new_obs, state_to_goal_filter)

		# new_goal = policy_high.select_action(np.asarray(new_obs))
		# env.env.env.set_goal(new_obs[:2] + new_goal)

		total_reward += reward
		total_reward_intrinsic += r_i

		env.render()
		last_time = time.time()
		while time.time() - last_time < step_dur:
			pass

		goal = new_goal
		obs = new_obs
		episode_timesteps += 1

	env.env.close()
	env.close()

	print(total_reward)
	print(total_reward_intrinsic)

	for file in os.listdir(video_directory):
		if (file.endswith(".mp4")):
			video_path = "{}.mp4".format(video_directory)
			os.rename("{}/{}".format(video_directory, file), video_path)
			print("Saved as {0}".format(video_path))

	shutil.rmtree(video_directory)




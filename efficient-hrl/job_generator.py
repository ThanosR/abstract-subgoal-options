import os

TRAIN_BOTH = 0
TRAIN_HIGH = 1
TRAIN_LOW = 2
TRAIN_FLAT = 3



local_job = False
# env_name = "DoomGridworldGather-v0"
env_name = "GridworldCollect-v0"
train_id = TRAIN_HIGH

env_folder = "./jobs/{}".format(env_name)

if not os.path.exists(env_folder):
	os.makedirs(env_folder)

if not local_job:
	with open('./jobs/job_prefix.txt', 'r') as myfile:
		job = myfile.read()
else:
	job = "#!/bin/sh\n"

job+="\n"


if train_id == TRAIN_HIGH:

	discounts = [0.8, 0.9]
	# expl_noise_g = [0.2, 0.4]
	expl_noise_g = [0.2]
	C = [5, 10]
	lr_as = [0.001, 0.0001]
	lr_cs = [0.001, 0.0001]

	i  = 1

	for d in discounts:
		for c in C:
			for lr_a in lr_as:
				for lr_c in lr_cs:
					for expl_g in expl_noise_g:
						run_string = "" if local_job else "srun -u "

						run_string += "python3 main_HIRO.py --env_name={} ".format(env_name)
						run_string += "--filename_extension=low_fixed "
						run_string += "--start_timesteps=5000 --max_timesteps=100000 "
						run_string += "--discount_h={} ".format(d)
						run_string += "--c={} ".format(c)
						run_string += "--lr_actor_h={} ".format(lr_a)
						run_string += "--lr_critic_h={} ".format(lr_c)
						run_string += "--expl_noise_h={} ".format(expl_g)
						run_string += " --run_directory={}".format("Train_high")

						job += run_string + "\n"


	job_name = "job_train_high.sh"


elif train_id == TRAIN_LOW:


	discounts = [0.8, 0.9]
	expl_noise = [0.2] #, 0.4]
	C = [5, 10]
	lr_as = [0.001, 0.0001]
	lr_cs = [0.001, 0.0001]

	i  = 1

	for d in discounts:
		for c in C:
			for lr_a in lr_as:
				for lr_c in lr_cs:
					for expl in expl_noise:

						run_string = "srun -u python3 main_HIRO.py --env_name={} ".format(env_name)
						run_string += "--filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 "
						run_string += "--discount={} ".format(d)
						run_string += "--c={} ".format(c)
						run_string += "--lr_actor={} ".format(lr_a)
						run_string += "--lr_critic={} ".format(lr_c)
						run_string += "--expl_noise={} ".format(expl)
						run_string += " run_directory={}".format("L1_int")
						job += run_string + "\n"

	job_name = "job_train_low.sh"



elif train_id == TRAIN_BOTH:

	c = 5
	high_parameters = [[0.8, 0.2, 0.0001, 0.001],
	                   [0.8, 0.2, 0.001,  0.001],
	                   [0.9, 0.2, 0.001,  0.0001]]
	low_parameters = [[0.8, 0.2, 0.0001, 0.0001],
	                  [0.9, 0.2, 0.0001, 0.0001]]


	for high_setup in high_parameters:
		for low_setup in low_parameters:
			run_string = "srun -u python3 main_HIRO.py --start_timesteps=3000 --c={} --max_timesteps=50000 --discount={} --expl_noise={} --lr_actor={} --lr_critic={} --discount_h={} --expl_noise_h={} --lr_actor_h={} --lr_critic_h={}".format(
				c, low_setup[0], low_setup[1], low_setup[2], low_setup[3], high_setup[0], high_setup[1], high_setup[2], high_setup[3]
			)
			job+= run_string + "\n"

	job_name = "job_all.sh"


elif train_id == TRAIN_FLAT:

	discounts = [0.8, 0.9]
	expl_noise = [0.2, 0.4]
	lr_as = [0.001, 0.0001]
	lr_cs = [0.001, 0.0001]

	i  = 1

	for d in discounts:
		for lr_a in lr_as:
			for lr_c in lr_cs:
				for expl in expl_noise:

					run_string = "srun -u python3 main_TD3.py --start_timesteps=3000 --max_timesteps=100000 --discount={} --lr_a={} --lr_c={} --expl_noise={}".format(d, lr_a, lr_c, expl)
					job += "\n" + run_string

	job_name = "job_flat.sh"


if local_job: job_name = "local_" + job_name

print(job)
with open("{}/{}".format(env_folder, job_name), 'w') as myfile:
	myfile.write(job)


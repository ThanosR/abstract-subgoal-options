import gym
import environments
import pickle
import time
import numpy as np
import os
import utils
import TD3
from argparse import Namespace
import FixedPolicies
import argparse


def RepresentsInt(s):
	try:
		int(s)
		return True
	except ValueError:
		return False


def RepresentsFloat(s):
	try:
		float(s)
		return True
	except ValueError:
		return False


def read_args(args_path):
	args = {}
	with open(args_path, "r") as args_txt:
		arguments = args_txt.readlines()
		for line in arguments:
			tokens = line.split()
			key = tokens[0][:-1]
			value = tokens[1] if len(tokens) == 2 else ""

			if RepresentsInt(value):
				value = int(value)
			elif RepresentsFloat(value):
				value = float(value)

			args[key] = value

		args = Namespace(**args)

		for k in sorted(args.__dict__.keys()):
			print("{}: {}".format(k, args.__dict__[k]))
	return args



if __name__ == "__main__":

	#region args
	parser = argparse.ArgumentParser()

	parser.add_argument("--source_dir", default="")  # OpenAI gym environment name

	args = parser.parse_args()


	source_dir = args.source_dir

	args = read_args("{}/args.txt".format(source_dir))
	model_weights_directory = "{}/models".format(source_dir)

	env, goal_space, state_to_goal_filter, normalizer_high, normalizer_low = utils.setup_env(args)

	observation_dim = env.observation_space.shape[0]
	goal_dim = goal_space.shape[0]
	max_goal = goal_space.high
	action_dim = env.action_space.n
	max_action = None
	c = args.c

	if "HIRO" in source_dir:

		if "low_fixed" in source_dir:
			policy_high = TD3.TD3(observation_dim, goal_dim, max_goal, lr_a=args.lr_actor_h, lr_c=args.lr_critic_h)
			policy_low = FixedPolicies.GridworldGatherPolicyLow(action_dim, env.env)
			args.expl_noise = 0
			print("Running Low fixed (pathfinding), learning only High policy")

			policy_high.load("high", model_weights_directory)

		elif "high_fixed" in source_dir:
			policy_low = TD3.TD3(observation_dim + goal_dim, action_dim, max_action, lr_a=args.lr_actor,
			                     lr_c=args.lr_critic)
			policy_high = FixedPolicies.GridworldGatherPolicyHigh(env.env)
			args.expl_noise_h = 0
			print("Running high fixed (closest pickup), learning only Low policy")

		else:
			policy_low = TD3.TD3(observation_dim + goal_dim, action_dim, max_action, lr_a=args.lr_actor,
			                     lr_c=args.lr_critic)
			policy_high = TD3.TD3(observation_dim, goal_dim, max_goal, lr_a=args.lr_actor_h, lr_c=args.lr_critic_h)
			print("learning both policies, Low and High")

	elif "TD3" in source_dir:
		pass

	obs = env.reset()
	goal = policy_high.select_action(np.asarray(obs))

	done = False
	episode_timesteps = 0

	total_reward = 0
	total_reward_intrinsic = 0

	state_action_history = []


	while not done:
		action = policy_low.select_action(np.append(np.array(obs), goal))

		new_obs, reward, done, _ = env.step(action if max_action is not None else np.argmax(action))

		r_i = utils.intrinsic_reward(obs, goal, action, new_obs, state_to_goal_filter, env.observation_space)

		if (episode_timesteps + 1) % c == 0:
			new_goal = policy_high.select_action(np.asarray(new_obs))
		else:
			new_goal = utils.h(obs, goal, new_obs, state_to_goal_filter)

		total_reward += reward
		total_reward_intrinsic += r_i

		state_action_history.append([obs, goal, action, reward, r_i, done])
		goal,obs = new_goal, new_obs
		episode_timesteps += 1


	with open("{}/eval_history.pkl".format(source_dir), 'wb+') as history_file:
		pickle.dump([state_action_history], history_file)

	env.env.close()
	env.close()


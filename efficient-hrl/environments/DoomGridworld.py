import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
from os import path
import math
import random
import types

# from pyglet.gl import *

A_NOTHING = 0
A_MOVE_FORWARDS = 1
A_MOVE_BACKWARDS = 2
A_STRAFE_LEFT = 3
A_STRAFE_RIGHT = 4
A_ROTATE_LEFT = 5
A_ROTATE_RIGHT = 6
A_SHOOT = 7

ROTATION_RIGHT = 0
ROTATION_UP = 1
ROTATION_LEFT = 2
ROTATION_DOWN = 3

AMMO_MAX = 10


GRID_FREE = 0
GRID_WALL = 1
GRID_PLAYER = 2
GRID_ENEMY = 3
GRID_HEALTH = 4
GRID_AMMO = 5
GRID_TOMATO = 6
GRID_CUCUMBER = 7
GRID_COLLECTOR = 8



def flatten(l):
	return [item for sublist in l for item in sublist]

class DoomGridworld(gym.Env):
	"""

	"""
	metadata = {
		'render.modes' : ['human', 'rgb_array'],
		'video.frames_per_second' : 10
	}

	render_objects = []


	def __init__(self):
		self.TILE_SIZE = 20

		if self.action_space == None:
			self.action_space = spaces.Discrete(5)

		self.grid = np.zeros((self.bounds_X, self.bounds_Y), dtype=int)

		self.VIEWPORT_W = self.TILE_SIZE * self.bounds_X
		self.VIEWPORT_H = self.TILE_SIZE * self.bounds_Y

		self.viewer = None

		self.goal = None

		self.seed()



	def seed(self, seed=None):
		self.np_random, seed = seeding.np_random(seed)
		return [seed]


	def reset(self):
		self.episode_steps = 0

		self.grid = np.copy(self.default_grid)

		for gameObject in self.gameObjects:
			gameObject.reset()
			self.grid[gameObject.pos[0], gameObject.pos[1]] = gameObject.grid_type

		state = self.to_observation(self)

		self.state = np.asarray(state,dtype=int) #/ self.observation_space.nvec

		return self.state


	def step(self,u):
		self.episode_steps += 1
		self.u = u
		self.reward = 0

		for gameObject in self.gameObjects:
			gameObject.update()

		self.terminal, terminal_reward = self.terminal_function(self)
		self.reward += terminal_reward

		state = self.to_observation(self)
		self.state = np.asarray(state, dtype=float)
		return self.state, self.reward, self.terminal, {}


	def render(self, mode='human'):

		if self.viewer is None:
			from gym.envs.classic_control import rendering
			self.viewer = rendering.Viewer(self.VIEWPORT_W,self.VIEWPORT_H + 100)
			self.viewer.set_bounds(0,self.VIEWPORT_W,0,self.VIEWPORT_H + 100)

			label = Text("", font_name='Times New Roman',
			                               font_size=36,
			                               x=self.viewer.window.width // 2,
			                               y=self.viewer.window.height - 18,
			                               anchor_x='center', anchor_y='center', color=[0, 0, 0, 255])

			def update_ep_text(self, env):
				self.text = str(env.episode_steps)

			label.update = types.MethodType(update_ep_text, label)

			self.render_objects.append(label)


		# Draw grid
		for i in range(self.bounds_Y):
			self.viewer.draw_line((0, i * self.TILE_SIZE), (self.VIEWPORT_W, i * self.TILE_SIZE), color=(0.2, 0.2, 0.2))

		for i in range(self.bounds_X):
			self.viewer.draw_line((i * self.TILE_SIZE, 0), (i * self.TILE_SIZE, self.VIEWPORT_H), color=(0.2, 0.2, 0.2))

		#Draw walls
		for blocked_tile in self.grid_walled:
			x,y = blocked_tile[0], blocked_tile[1]

			self.viewer.draw_polygon([
				(x * self.TILE_SIZE, (y) * self.TILE_SIZE),
				((x + 1) * self.TILE_SIZE, (y) * self.TILE_SIZE),
				((x + 1) * self.TILE_SIZE, (y+1) * self.TILE_SIZE),
				(x * self.TILE_SIZE, (y+1) * self.TILE_SIZE)]
				, color = (0.8, 0.8, 0.9) )



		for gameObject in self.gameObjects:
			gameObject.render(self.viewer)



		for render_object in self.render_objects:
			render_object.update(self)
			self.viewer.add_onetime(render_object)

		# self.label.text = str(self.episode_steps)
		# self.label.update(self)
		# self.viewer.add_onetime(self.label)

		# Draw goal
		if self.goal is not None:
			x, y = self.goal[:2]

			x = np.clip(x, 0, self.observation_space.nvec[0] - 1)
			y = np.clip(y, 0, self.observation_space.nvec[1] - 1)

			self.viewer.draw_polygon([
				(x * self.TILE_SIZE, (y) * self.TILE_SIZE),
				((x + 1) * self.TILE_SIZE, (y) * self.TILE_SIZE),
				((x + 1) * self.TILE_SIZE, (y + 1) * self.TILE_SIZE),
				(x * self.TILE_SIZE, (y + 1) * self.TILE_SIZE)]
				, color=(0, 0.8, 0))


		return self.viewer.render(return_rgb_array = mode=='rgb_array')


	def close(self):
		if self.viewer: self.viewer.close()


	def get_free_pos(self):
		posX = random.randint(0, self.bounds_X - 1)
		posY = random.randint(0, self.bounds_Y - 1)
		while(self.grid[posX,posY] != GRID_FREE):
			posX = random.randint(0, self.bounds_X - 1)
			posY = random.randint(0, self.bounds_Y - 1)
		return posX, posY


	def walkable_position(self, pos):
		x = pos[0]
		y = pos[1]

		if x < 0 or x >= self.bounds_X or y < 0 or y >= self.bounds_Y \
				or self.grid[x, y] == GRID_WALL \
				or self.grid[x, y] == GRID_ENEMY:
			return False
		return True

	def set_goal(self, goal):
		self.goal = goal



class DoomGridworldKill(DoomGridworld):


	def __init__(self):

		self.bounds_X = N_W = 19
		self.bounds_Y = N_H = 19

		self.default_grid = np.zeros((self.bounds_X, self.bounds_Y), dtype=int)

		# Make Walls
		for x in range(0, self.bounds_X, 6):
			self.default_grid[x, :] = GRID_WALL
		for y in range(0, self.bounds_Y, 6):
			self.default_grid[:, y] = GRID_WALL
		# Horizontal doors
		for y in range(6, self.bounds_Y - 1, 6):
			for x in range(3, self.bounds_X, 6):
				self.default_grid[x, y] = GRID_FREE
		# Vertical doors
		for y in range(3, self.bounds_Y, 6):
			for x in range(6, self.bounds_X - 1, 6):
				self.default_grid[x, y] = GRID_FREE

		self.grid_walled = list(zip(*np.where(self.default_grid == GRID_WALL)))

		self.enemies = []
		#Populate enemies
		for x in range(3, self.bounds_X - 1, 6):
			for y in range(3, self.bounds_Y - 1, 6):
				if not (x == 9 and y == 9):
					enemy = Enemy(self, x, y, 0, 100, shoot_points=0, kill_points=1)
					self.enemies.append(enemy)

		self.player = Player(self, 9, 9, 1, 100, AMMO_MAX)

		self.pickups = [Pickup(self,'ammo', 7, 11, pickup_points=0)]

		# [posX, posY, rotation, health, ammo, ammoX, ammoY] + 8 * [enemy_health]
		# self.observation_space = spaces.MultiDiscrete(np.asarray([N_W, N_H, 3, 100, AMMO_MAX] + [N_W, N_H] * len(self.pickups) + [100] * len(self.enemies)))
		self.observation_space = spaces.MultiDiscrete(np.asarray([N_W, N_H,1,1,1,1, 100, AMMO_MAX] + [N_W, N_H] * len(self.pickups) + [100] * len(self.enemies)))


		def to_observation(env):
			player = env.player
			pickups = env.pickups
			enemies = env.enemies

			rot_list = [0, 0, 0, 0]
			rot_list[self.player.rotation] = 1

			return [player.pos[0], player.pos[1]] + rot_list + [player.health, player.ammo] \
			+ flatten([pickup.pos.tolist() for pickup in pickups]) \
			+ [enemy.health for enemy in enemies]


		def killed_all_enemies(env):
			player = env.player
			enemies = env.enemies
			if not [True for enemy in enemies if enemy.isAlive()]:
				return True, 5
			return False, 0


		self.to_observation = to_observation
		self.terminal_function = killed_all_enemies

		self.gameObjects = [self.player]
		self.gameObjects += self.enemies
		self.gameObjects += self.pickups

		super().__init__()


	def shoot_bullet(self, bullet_pos, shoot_dir):

		while bullet_pos[0] >= 0 and bullet_pos[0] < self.bounds_X \
				and bullet_pos[1] >= 0 and bullet_pos[1] < self.bounds_Y:

			# Shot wall
			if self.grid[bullet_pos[0], bullet_pos[1]] == GRID_WALL:
				break

			# Shot enemy
			for enemy in self.enemies:
				if enemy.pos[0] == bullet_pos[0] \
						and enemy.pos[1] == bullet_pos[1] \
						and enemy.isAlive():

					self.reward += enemy.shoot_bullet()
					if not enemy.isAlive():
						self.grid[bullet_pos[0], bullet_pos[1]] = GRID_FREE
						self.reward += enemy.kill_points
					break

			bullet_pos += shoot_dir



class DoomGridworldGather(DoomGridworld):
	"""
	Collect 8 health packs placed in specific locations around the grid
	"""

	def __init__(self):

		self.bounds_X = N_W = 19
		self.bounds_Y = N_H = 19

		self.default_grid = np.zeros((self.bounds_X, self.bounds_Y), dtype=int)

		# Make Walls
		# for x in range(0, self.bounds_X, 6):
		# 	self.default_grid[x, :] = GRID_WALL
		# for y in range(0, self.bounds_Y, 6):
		# 	self.default_grid[:, y] = GRID_WALL
		# # Horizontal doors
		# for y in range(6, self.bounds_Y - 1, 6):
		# 	for x in range(3, self.bounds_X, 6):
		# 		self.default_grid[x, y] = GRID_FREE
		# # Vertical doors
		# for y in range(3, self.bounds_Y, 6):
		# 	for x in range(6, self.bounds_X - 1, 6):
		# 		self.default_grid[x, y] = GRID_FREE

		self.grid_walled = list(zip(*np.where(self.default_grid == GRID_WALL)))


		self.player = Player(self, 9, 9, 1, 100, AMMO_MAX)

		self.pickups = []
		for x in range(3, self.bounds_X - 1, 6):
			for y in range(3, self.bounds_Y - 1, 6):
				if not (x == 9 and y == 9):
					pickup = Pickup(self, 'ammo', x, y, pickup_points=1)
					self.pickups.append(pickup)


		#-----------------------------
		# BIG OBSERVATION SPACE - DO NOT DELETE
		# # [posX, posY, rotation, health, ammo, ammoX, ammoY] + 8 * [enemy_health]
		# self.observation_space = spaces.MultiDiscrete(np.asarray([N_W, N_H,1,1,1,1, 100, AMMO_MAX] + [N_W, N_H] * len(self.pickups) + [100] * len(self.enemies)))
		#
		# def to_observation(player, pickups, enemies):
		# 	rot_list = [0, 0, 0, 0]
		# 	rot_list[self.player.rotation] = 1
		#
		# 	return [player.pos[0], player.pos[1]] + rot_list + [player.health, player.ammo] \
		# 	+ flatten([pickup.pos.tolist() for pickup in pickups]) \
		# 	+ [enemy.health for enemy in enemies]
		#
		# self.to_observation = to_observation
		#-------------------------------


		# [posX, posY, rotation +  8 * [was_picked]
		self.observation_space = spaces.MultiDiscrete(np.asarray([N_W, N_H,1,1,1,1] + len(self.pickups) * [1]))

		def to_observation(env):
			player = env.player
			pickups = env.pickups
			rot_list = [0, 0, 0, 0]
			rot_list[self.player.rotation] = 1

			return [player.pos[0], player.pos[1]] + rot_list + [int(not pickup.was_picked) for pickup in pickups]




		# [posX, posY, rotation]
		# self.observation_space = spaces.MultiDiscrete(np.asarray([N_W, N_H, 1,1,1,1]))
		#
		#
		# def to_observation(player, pickups, enemies):
		#
		# 	rot_list = [0, 0, 0, 0]
		# 	rot_list[self.player.rotation] = 1
		#
		# 	return [player.pos[0], player.pos[1]] + rot_list

		def picked_all_pickups(env):
			player = env.player
			pickups = env.pickups
			if not [True for pickup in pickups if not pickup.was_picked]:
				return True, 5
			return False, 0


		self.to_observation = to_observation
		self.terminal_function = picked_all_pickups

		self.gameObjects = [self.player]
		self.gameObjects += self.pickups

		super().__init__()



class GridworldCollect(DoomGridworld):
	"""
	Collect tomatoes and cucumbers in the grid and place them back in the basket
	"""

	def __init__(self):

		self.bounds_X = N_W = 19
		self.bounds_Y = N_H = 19

		self.default_grid = np.zeros((self.bounds_X, self.bounds_Y), dtype=int)

		self.grid_walled = list(zip(*np.where(self.default_grid == GRID_WALL)))

		self.player = Player(self, 9, 9, 1, 100, AMMO_MAX)

		self.pickups = []

		# Tomatoes initialization
		self.pickups.append(Pickup(self, 'tomato', 11 , 16, pickup_points=1))
		self.pickups.append(Pickup(self, 'tomato', 2 , 7, pickup_points=1))
		self.pickups.append(Pickup(self, 'tomato', 16 , 7, pickup_points=1))
		self.pickups.append(Pickup(self, 'tomato', 2 , 13, pickup_points=1))
		self.pickups.append(Pickup(self, 'tomato', 12, 4, pickup_points=1))

		# Cucumbes initialization
		self.pickups.append(Pickup(self, 'cucumber', 7 , 16, pickup_points=1))
		self.pickups.append(Pickup(self, 'cucumber', 5 , 10, pickup_points=1))
		self.pickups.append(Pickup(self, 'cucumber', 12 , 10, pickup_points=1))
		self.pickups.append(Pickup(self, 'cucumber', 16 , 13, pickup_points=1))
		self.pickups.append(Pickup(self, 'cucumber', 5 , 4, pickup_points=1))


		self.tomato_collector = Collector(self, 'tomato', 7, 1)
		self.cucumber_collector = Collector(self, 'cucumber', 12, 1)


		# [posX, posY, rotation +  8 * [was_picked]
		self.observation_space = spaces.MultiDiscrete(np.asarray([N_W, N_H] + [5, 5] + len(self.pickups) * [1]))

		def to_observation(env):
			player, pickups = env.player, env.pickups
			rot_list = [0, 0, 0, 0]
			rot_list[self.player.rotation] = 1

			return [player.pos[0], player.pos[1]] + [env.tomato_collector.items_collected] + [env.cucumber_collector.items_collected] + [int(not pickup.was_picked) for pickup in pickups]

		def picked_all_pickups(env):
			return False, 0


		self.to_observation = to_observation
		self.terminal_function = picked_all_pickups


		self.gameObjects = [self.player]
		self.gameObjects += self.pickups
		self.gameObjects += [self.tomato_collector, self.cucumber_collector]



		tomatoes_label = Text("", font_name='Times New Roman', font_size=30, x= 25, y= 400, anchor_x='center', anchor_y='center', color=[255, 0, 0, 255])
		def update_tomatoes(self, env): self.text = str(env.tomato_collector.items_collected)
		tomatoes_label.update = types.MethodType(update_tomatoes, tomatoes_label)

		cucumbers_label = Text("", font_name='Times New Roman', font_size=30, x=60, y= 400, anchor_x='center', anchor_y='center', color=[0, 255, 0, 255])
		def update_cucumbers(self, env): self.text = str(env.cucumber_collector.items_collected)
		cucumbers_label.update = types.MethodType(update_cucumbers, cucumbers_label)

		self.render_objects.extend([tomatoes_label, cucumbers_label])

		super().__init__()





class Player(object):

	def __init__(self, env, default_posX = 0, default_posY = 0, default_rotation = 0, default_health = 100, default_ammo = 5):

		self.env = env

		self.default_posX = default_posX
		self.default_posY = default_posY
		self.default_rotation = default_rotation
		self.default_health = default_health
		self.default_ammo = default_ammo

		self.pos = np.asarray([default_posX, default_posY])

		self.image = None
		self.render_history = False
		self.grid_type = GRID_PLAYER

	def reset(self, posX = None, posY = None, rotation = None, health = None, ammo = None):

		posX = posX if posX else self.default_posX
		posY = posY if posY else self.default_posY
		rotation = rotation if rotation else self.default_rotation
		health = health if health else self.default_health
		ammo = ammo if ammo else self.default_ammo

		self.pos[0], self.pos[1] = posX, posY
		self.rotation = rotation
		self.health = health
		self.ammo = ammo

		self.history = []

	def update(self):
		u = self.env.u

		self.rotation = (self.rotation + (u == A_ROTATE_LEFT) - (u == A_ROTATE_RIGHT)) % 4

		lookDir = [0, 0]
		if self.rotation == ROTATION_RIGHT:
			lookDir = [1, 0]
		elif self.rotation == ROTATION_UP:
			lookDir = [0, 1]
		elif self.rotation == ROTATION_LEFT:
			lookDir = [-1, 0]
		elif self.rotation == ROTATION_DOWN:
			lookDir = [0, -1]
		lookDir = np.array(lookDir)

		inputX = int(u == A_STRAFE_RIGHT) - int(u == A_STRAFE_LEFT)
		inputY = int(u == A_MOVE_FORWARDS) - int(u == A_MOVE_BACKWARDS)

		next_pos = self.pos.copy()
		next_pos += lookDir * inputY
		# Vector (x,y) rotated to the right -> (y, -x)
		next_pos += np.array([lookDir[1], -lookDir[0]]) * inputX
		if self.env.walkable_position(next_pos):
			self.pos[0], self.pos[1] = next_pos[0], next_pos[1]

		# Shooting
		if u == A_SHOOT and self.ammo > 0:
			self.ammo -= 1
			bullet_pos = self.pos.copy()
			self.env.shoot_bullet(bullet_pos, lookDir)


	def render(self, viewer):

		if self.image is None:
			from gym.envs.classic_control import rendering
			fname = path.join(path.dirname(__file__), "assets/soldier1_gun.png")
			self.image = rendering.Image(fname, self.env.TILE_SIZE, self.env.TILE_SIZE)
			self.transform = rendering.Transform()
			self.image.add_attr(self.transform)
			self.image.set_color(1,1,1)

		if self.isAlive():
			# self.render_at(viewer, self.pos, self.rotation, )
			self.transform.translation = self.env.TILE_SIZE * self.pos + np.array([self.env.TILE_SIZE, self.env.TILE_SIZE]) / 2
			self.transform.rotation = self.rotation * math.pi / 2
			viewer.add_onetime(self.image)

		if self.render_history:
			from gym.envs.classic_control import rendering
			fname = path.join(path.dirname(__file__), "assets/soldier1_gun.png")
			ghost_image = rendering.Image(fname, self.env.TILE_SIZE, self.env.TILE_SIZE)
			ghost_transform = rendering.Transform()
			ghost_image.add_attr(ghost_transform)
			ghost_image._color.vec4 = (1, 1, 1, 0.2)

			ghost_transform.translation = self.env.TILE_SIZE * self.pos + np.array(
				[self.env.TILE_SIZE, self.env.TILE_SIZE]) / 2
			ghost_transform.rotation = self.rotation * math.pi / 2
			viewer.add_geom(ghost_image)
			self.history.append([self.pos, self.rotation])


	def isAlive(self):
		return self.health > 0



class Enemy(object):

	def __init__(self, env, default_posX = 0, default_posY = 0, default_rotation = 0, default_health = 100, shoot_points = 0, kill_points = 0):

		self.env = env

		self.default_posX = default_posX
		self.default_posY = default_posY
		self.default_rotation = default_rotation
		self.default_health = default_health
		self.shoot_points = shoot_points
		self.kill_points = kill_points

		self.pos = np.asarray([default_posX, default_posY])
		self.image = None
		self.grid_type = GRID_ENEMY



	def reset(self, posX = None, posY = None , rotation = None, health = None):

		posX = posX if posX else self.default_posX
		posY = posY if posY else self.default_posY
		rotation = rotation if rotation else self.default_rotation
		health = health if health else self.default_health


		self.pos[0], self.pos[1] = posX, posY
		self.rotation = rotation
		self.health = health


	def render(self, viewer):

		if self.image is None:
			from gym.envs.classic_control import rendering
			fname = path.join(path.dirname(__file__), "assets/zoimbie1_stand.png")
			self.image = rendering.Image(fname, self.env.TILE_SIZE, self.env.TILE_SIZE)
			self.transform = rendering.Transform()
			self.image.add_attr(self.transform)
			self.image.set_color(1,0.4,0.4)

		if self.isAlive():
			self.transform.translation = self.env.TILE_SIZE * self.pos + np.array([self.env.TILE_SIZE, self.env.TILE_SIZE]) / 2
			self.transform.rotation = self.rotation * math.pi / 2
			viewer.add_onetime(self.image)

	def isAlive(self):
		return self.health > 0


	def shoot(self):
		self.health = 0
		return self.shoot_points



class Pickup(object):

	def __init__(self, env, type, default_posX = None, default_posY = None, pickup_points = 0):

		self.default_posX = default_posX
		self.default_posY = default_posY
		self.pickup_points = pickup_points

		self.env = env
		self.type = type
		self.pos = np.asarray([default_posX, default_posY])

		self.image = None

		if self.type == 'health':
			self.grid_type = GRID_HEALTH
		elif self.type == 'ammo':
			self.grid_type = GRID_AMMO
		elif self.type == 'tomato':
			self.grid_type = GRID_TOMATO
		elif self.type == 'cucumber':
			self.grid_type = GRID_CUCUMBER

	def reset(self, posX = None, posY = None):

		posX = posX if posX else self.default_posX
		posY = posY if posY else self.default_posY

		self.pos[0], self.pos[1] = posX, posY
		self.was_picked = False


	def render(self, viewer):

		if self.image is None:
			from gym.envs.classic_control import rendering

			if self.type == 'health':
				fname = path.join(path.dirname(__file__), "assets/health_pack.jpg")
			elif self.type == 'ammo':
				fname = path.join(path.dirname(__file__), "assets/health_pack.jpg")
			elif self.type == 'tomato':
				fname = path.join(path.dirname(__file__), "assets/tomato.png")
			elif self.type == 'cucumber':
				fname = path.join(path.dirname(__file__), "assets/cucumber.png")

			self.image = rendering.Image(fname, self.env.TILE_SIZE, self.env.TILE_SIZE)
			self.transform = rendering.Transform()
			self.image.add_attr(self.transform)
			self.image.set_color(1,1,1)

		if not self.was_picked:
			self.transform.translation = self.env.TILE_SIZE * self.pos + np.array([self.env.TILE_SIZE, self.env.TILE_SIZE]) / 2
			viewer.add_onetime(self.image)


	def update(self):
		player = self.env.player
		env = self.env

		if player.pos[0] == self.pos[0] and player.pos[1] == self.pos[1]:
			self.onPickup(player)
			env.reward += self.pickup_points


	def onPickup(self, player):
		if self.type == 'health':
			player.health = 100
		elif self.type == 'ammo':
			player.ammo = AMMO_MAX
		elif self.type == 'tomato':
			self.env.tomato_collector.items_collected += 1
		elif self.type == 'cucumber':
			self.env.cucumber_collector.items_collected += 1

		self.was_picked = True
		self.pos[0] = self.pos[1] = -1



class Collector(object):

	def __init__(self, env, type, default_posX = None, default_posY = None):

		self.default_posX = default_posX
		self.default_posY = default_posY

		self.env = env
		self.type = type
		self.items_collected = 0
		self.pos = np.asarray([default_posX, default_posY])

		self.image = None
		self.grid_type = GRID_COLLECTOR



	def reset(self, posX = None, posY = None):

		posX = posX if posX else self.default_posX
		posY = posY if posY else self.default_posY
		self.items_collected = 0

		self.pos[0], self.pos[1] = posX, posY


	def render(self, viewer):

		if self.image is None:
			from gym.envs.classic_control import rendering

			fname = path.join(path.dirname(__file__), "assets/basket.png")

			self.image = rendering.Image(fname, self.env.TILE_SIZE, self.env.TILE_SIZE)
			self.transform = rendering.Transform()
			self.image.add_attr(self.transform)
			if self.type == 'tomato':
				self.image.set_color(0.8,0.2,0.2)
			elif self.type == 'cucumber':
				self.image.set_color(0.2,0.8,0.2)

		self.transform.translation = self.env.TILE_SIZE * self.pos + np.array([self.env.TILE_SIZE, self.env.TILE_SIZE]) / 2
		viewer.add_onetime(self.image)

	def update(self):
		player = self.env.player
		env = self.env

		if player.pos[0] == self.pos[0] and player.pos[1] == self.pos[1]:
			env.reward += self.items_collected
			self.items_collected = 0




import pyglet
class Text(pyglet.text.Label):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)


	def update(self, env):
		pass

	def render(self):
		self.draw()


import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
from os import path

class MyDiscretePendulumEnv(gym.Env):
    metadata = {
        'render.modes' : ['human', 'rgb_array'],
        'video.frames_per_second' : 30
    }

    def __init__(self):
        self.max_speed=8
        self.max_torque=2.
        self.dt=.05
        self.viewer = None

        high = np.array([1., 1., self.max_speed])
        self.action_space = spaces.Discrete(2)
        self.observation_space = spaces.Box(low=-high, high=high)

        self.seed()

        self.goal_color = (0.2, 1, 0.2)
        self.goal = None

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]


    def step(self,u):
        u = self.max_torque * (2*u - 1) # convert {0,1} to {-2,2}

        th, thdot = self.state # th := theta

        g = 10.
        m = 1.
        l = 1.
        dt = self.dt

        u = np.clip(u, -self.max_torque, self.max_torque)
        self.last_u = u # for rendering
        costs = angle_normalize(th)**2 + .1*thdot**2 + .001*(u**2)

        newthdot = thdot + (-3*g/(2*l) * np.sin(th + np.pi) + 3./(m*l**2)*u) * dt
        newth = th + newthdot*dt
        newthdot = np.clip(newthdot, -self.max_speed, self.max_speed) #pylint: disable=E1111

        self.state = np.array([newth, newthdot])
        return self._get_obs(), -costs, False, {}

    def reset(self):
        self.goal = None
        high = np.array([np.pi, 1])
        self.state = self.np_random.uniform(low=-high, high=high)
        self.last_u = None
        return self._get_obs()

    def _get_obs(self):
        theta, thetadot = self.state
        return np.array([np.cos(theta), np.sin(theta), thetadot])

    def render(self, mode='human'):

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(500,500)
            self.viewer.set_bounds(-2.2,2.2,-2.2,2.2)

            rod = rendering.make_capsule(1, .2)
            rod.set_color(.8, .3, .3)
            self.pole_transform = rendering.Transform()
            rod.add_attr(self.pole_transform)
            self.viewer.add_geom(rod)

            axle = rendering.make_circle(.05)
            axle.set_color(0,0,0)
            self.viewer.add_geom(axle)

            fname = path.join(path.dirname(__file__), "assets/clockwise.png")
            self.img = rendering.Image(fname, 1., 1.)
            self.imgtrans = rendering.Transform()
            self.img.add_attr(self.imgtrans)
            self.img.set_color(0,0,0)
            self.rod = rod



            goal_rod = rendering.make_capsule(1, .2)
            goal_rod.set_color(*self.goal_color)
            self.goal_pole_transform = rendering.Transform()
            goal_rod.add_attr(self.goal_pole_transform)
            self.goal_rod = goal_rod

            self.img2 = rendering.Image(fname, 1., 1.)
            self.imgtrans2 = rendering.Transform()
            self.img2.add_attr(self.imgtrans2)
            self.img2.set_color(*self.goal_color)


        self.viewer.add_onetime(self.img)
        self.pole_transform.set_rotation(self.state[0] + np.pi/2)
        if self.last_u:
            self.imgtrans.scale = (-self.last_u/2, np.abs(self.last_u)/2)

        if self.goal is not None:
            cos = self.goal[0]
            sin = self.goal[1]
            theta = np.arcsin(sin)
            if cos < 0:
                if theta > 0:
                    theta += np.pi / 2
                else:
                    theta -= np.pi /2

            self.goal_pole_transform.set_rotation(theta + np.pi/2)
            self.viewer.add_onetime(self.goal_rod)

            self.viewer.add_onetime(self.img2)
            goal_u = self.goal[2]
            self.imgtrans2.scale = (-goal_u / 4, np.abs(goal_u) / 4)

        return self.viewer.render(return_rgb_array = mode=='rgb_array')

    def set_goal(self, goal):
        self.goal = goal


def angle_normalize(x):
    return (((x+np.pi) % (2*np.pi)) - np.pi)



from gym.envs.registration import register

register(
    id='MyBipedalWalker-v0',
    entry_point='environments.MyBipedalWalker:MyBipedalWalker',
    tags={'wrapper_config.TimeLimit.max_episode_steps': 1600},
)

register(
    id='MyPendulum-v0',
    entry_point='environments.MyPendulum:MyPendulumEnv',
    tags={'wrapper_config.TimeLimit.max_episode_steps': 200},
)

register(
    id='MyDiscretePendulum-v0',
    entry_point='environments.MyDiscretePendulum:MyDiscretePendulumEnv',
    tags={'wrapper_config.TimeLimit.max_episode_steps': 200},
)

register(
    id='DoomGridworld-v0',
    entry_point='environments.DoomGridworld:DoomGridworld',
    tags={'wrapper_config.TimeLimit.max_episode_steps': 150},
)

register(
    id='DoomGridworldKill-v0',
    entry_point='environments.DoomGridworld:DoomGridworldKill',
    tags={'wrapper_config.TimeLimit.max_episode_steps': 150},
)

register(
    id='DoomGridworldGather-v0',
    entry_point='environments.DoomGridworld:DoomGridworldGather',
    tags={'wrapper_config.TimeLimit.max_episode_steps': 150},
)



register(
    id='GridworldCollect-v0',
    entry_point='environments.DoomGridworld:GridworldCollect',
    tags={'wrapper_config.TimeLimit.max_episode_steps': 200},
)
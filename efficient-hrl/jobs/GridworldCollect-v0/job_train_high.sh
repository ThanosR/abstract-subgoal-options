#!/bin/sh
#SBATCH -n 1
#SBATCH -p gpu
#SBATCH -t 6:00:00
#SBATCH -o out/j_%A.o
#SBATCH -e out/j_%A.e

module load python/3.5.2
module load cuda/8.0.44
module load cudnn/8.0-v6.0


srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.8 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.8 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.8 --c=5 --lr_actor_h=0.0001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.8 --c=5 --lr_actor_h=0.0001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.8 --c=10 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.8 --c=10 --lr_actor_h=0.001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.8 --c=10 --lr_actor_h=0.0001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.8 --c=10 --lr_actor_h=0.0001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.9 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.9 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.9 --c=5 --lr_actor_h=0.0001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.9 --c=5 --lr_actor_h=0.0001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.9 --c=10 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.9 --c=10 --lr_actor_h=0.001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.9 --c=10 --lr_actor_h=0.0001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=Train_high
srun -u python3 main_HIRO.py --env_name=GridworldCollect-v0 --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount_h=0.9 --c=10 --lr_actor_h=0.0001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=Train_high

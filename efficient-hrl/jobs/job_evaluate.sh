#!/bin/sh
#SBATCH -n 1
#SBATCH -p gpu_short
#SBATCH -t 00:05:00
#SBATCH -o out/eval_%A.o
#SBATCH -e out/eval_%A.e

module load python/3.5.2
module load cuda/8.0.44
module load cudnn/8.0-v6.0

srun -u python3 test_load.py --source_dir=./results/DoomGridworldGather-v0/train_both_best/HIRO_DoomGridworldGather-v0_20181022-0719





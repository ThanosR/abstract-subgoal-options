#!/bin/sh

python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.8 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.8 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.8 --c=5 --lr_actor_h=0.0001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.8 --c=5 --lr_actor_h=0.0001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.8 --c=10 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.8 --c=10 --lr_actor_h=0.001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.8 --c=10 --lr_actor_h=0.0001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.8 --c=10 --lr_actor_h=0.0001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.9 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.9 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.9 --c=5 --lr_actor_h=0.0001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.9 --c=5 --lr_actor_h=0.0001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.9 --c=10 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.9 --c=10 --lr_actor_h=0.001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.9 --c=10 --lr_actor_h=0.0001 --lr_critic_h=0.001 --expl_noise_h=0.2  --run_directory=L1_int
python3 main_HIRO.py --filename_extension=low_fixed --start_timesteps=5000 --max_timesteps=100000 --discount=0.9 --c=10 --lr_actor_h=0.0001 --lr_critic_h=0.0001 --expl_noise_h=0.2  --run_directory=L1_int

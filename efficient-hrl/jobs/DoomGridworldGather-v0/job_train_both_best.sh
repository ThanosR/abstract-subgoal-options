#!/bin/sh
#SBATCH -n 1
#SBATCH -p gpu
#SBATCH -t 12:00:00
#SBATCH -o out/both_best_%A.o
#SBATCH -e out/both_best_%A.e

module load python/3.5.2
module load cuda/8.0.44
module load cudnn/8.0-v6.0


srun -u python3 main_HIRO.py --start_timesteps=5000 --max_timesteps=500000 --discount=0.8 --expl_noise=0.2 --lr_actor=0.001 --lr_critic=0.001 --discount_h=0.99 --c=5 --lr_actor_h=0.001 --lr_critic_h=0.001 --expl_noise_h=0.2 --no_goal_resamples=4 --run_directory=train_best





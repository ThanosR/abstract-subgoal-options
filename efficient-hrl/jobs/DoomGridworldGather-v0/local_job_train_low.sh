#!/bin/sh

python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.8 --c=5 --lr_actor=0.001 --lr_critic=0.001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.8 --c=5 --lr_actor=0.001 --lr_critic=0.0001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.8 --c=5 --lr_actor=0.0001 --lr_critic=0.001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.8 --c=5 --lr_actor=0.0001 --lr_critic=0.0001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.8 --c=10 --lr_actor=0.001 --lr_critic=0.001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.8 --c=10 --lr_actor=0.001 --lr_critic=0.0001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.8 --c=10 --lr_actor=0.0001 --lr_critic=0.001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.8 --c=10 --lr_actor=0.0001 --lr_critic=0.0001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.9 --c=5 --lr_actor=0.001 --lr_critic=0.001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.9 --c=5 --lr_actor=0.001 --lr_critic=0.0001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.9 --c=5 --lr_actor=0.0001 --lr_critic=0.001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.9 --c=5 --lr_actor=0.0001 --lr_critic=0.0001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.9 --c=10 --lr_actor=0.001 --lr_critic=0.001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.9 --c=10 --lr_actor=0.001 --lr_critic=0.0001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.9 --c=10 --lr_actor=0.0001 --lr_critic=0.001 --expl_noise=0.2
python3 main_HIRO.py --filename_extension=high_fixed --start_timesteps=3000 --max_timesteps=50000 --discount=0.9 --c=10 --lr_actor=0.0001 --lr_critic=0.0001 --expl_noise=0.2

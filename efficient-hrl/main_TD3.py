import socket
runs_locally = socket.gethostname() == "thanos-Alienware-15-R3"

import numpy as np
import torch
import gym
import argparse
import os

import utils
import ReplayBuffer
import TD3
import OurDDPG
import DDPG
import environments

from tensorboardX import SummaryWriter

import time

np.set_printoptions(precision=4)
np.set_printoptions(suppress=True)
np.set_printoptions(linewidth=130)


# Runs policy for X episodes and returns average reward
def evaluate_policy(policy, eval_episodes=10):
	avg_reward = 0.
	for ep in range(eval_episodes):
		obs = env.reset()
		done = False
		while not done:
			action = policy.select_action(np.array(obs))
			obs, reward, done, _ = env.step(action if max_action is not None else np.argmax(action))
			avg_reward += reward

			if runs_locally and ep == 0:
				env.render()

	avg_reward /= eval_episodes

	print("---------------------------------------")
	print("Evaluation over %d episodes: %f" % (eval_episodes, avg_reward))
	print("---------------------------------------")
	return avg_reward

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--policy_name", default="TD3")					# Policy name
	parser.add_argument("--env_name", default="HalfCheetah-v1")			# OpenAI gym environment name
	parser.add_argument("--seed", default=0, type=int)					# Sets Gym, PyTorch and Numpy seeds
	parser.add_argument("--start_timesteps", default=1e4, type=int)		# How many time steps purely random policy is run for
	parser.add_argument("--eval_freq", default=5e3, type=float)			# How often (time steps) we evaluate
	parser.add_argument("--max_timesteps", default=1e6, type=float)		# Max time steps to run environment for
	parser.add_argument("--save_models", action="store_true")			# Whether or not models are saved
	parser.add_argument("--expl_noise", default=0.1, type=float)		# Std of Gaussian exploration noise
	parser.add_argument("--batch_size", default=100, type=int)			# Batch size for both actor and critic
	parser.add_argument("--discount", default=0.99, type=float)			# Discount factor
	parser.add_argument("--tau", default=0.005, type=float)				# Target network update rate
	parser.add_argument("--policy_noise", default=0.2, type=float)		# Noise added to target policy during critic update
	parser.add_argument("--noise_clip", default=0.5, type=float)		# Range to clip target policy noise
	parser.add_argument("--policy_freq", default=2, type=int)			# Frequency of delayed policy updates
	parser.add_argument("--lr_a", default=0.001, type=float)  # Frequency of delayed policy updates
	parser.add_argument("--lr_c", default=0.001, type=float)  # Frequency of delayed policy updates

	args = parser.parse_args()
	args.save_models = True

	args.env_name = 'DoomGridworldGather-v0'

	file_name = "%s_%s_%s" % (args.env_name, args.policy_name, time.strftime("%Y%m%d-%H%M"))

	print("---------------------------------------")
	print("Settings: %s" % (file_name))
	print("---------------------------------------")

	if not os.path.exists("./results"):
		os.makedirs("./results")
	if args.save_models and not os.path.exists("./pytorch_models"):
		os.makedirs("./pytorch_models")

	writer = SummaryWriter("./results/{}/{}".format(args.env_name, file_name))

	with open('./results/{}/{}/args.txt'.format(args.env_name, file_name), 'w') as output:
		for k in sorted(args.__dict__.keys()):
			output.writelines("{}: {}\n".format(k, args.__dict__[k]))
			print("{}: {}".format(k, args.__dict__[k]))


	env = gym.make(args.env_name)

	# Set seeds
	env.seed(args.seed)
	torch.manual_seed(args.seed)
	np.random.seed(args.seed)

	state_dim = env.observation_space.shape[0]

	if type(env.action_space) is gym.spaces.Discrete:
		action_dim = env.action_space.n
		max_action = None
	elif type(env.action_space) is gym.spaces.Box:
		action_dim = env.action_space.shape[0]
		max_action = env.action_space.high

	# Initialize policy
	if args.policy_name == "TD3": policy = TD3.TD3(state_dim, action_dim, max_action, lr_a = args.lr_a, lr_c = args.lr_c)
	elif args.policy_name == "OurDDPG": policy = OurDDPG.DDPG(state_dim, action_dim, max_action)
	elif args.policy_name == "DDPG": policy = DDPG.DDPG(state_dim, action_dim, max_action)

	replay_buffer = ReplayBuffer.ReplayBuffer()
	
	# Evaluate untrained policy
	evaluations = [evaluate_policy(policy)] 

	total_timesteps = 0
	timesteps_since_eval = 0
	episode_num = 0
	done = True 

	state_action_history = []

	while total_timesteps < args.max_timesteps:
		
		if done: 

			if total_timesteps != 0: 
				print(("Total T: %d Episode Num: %d Episode T: %d Reward: %f") % (total_timesteps, episode_num, episode_timesteps, episode_reward))
				if args.policy_name == "TD3":
					loss_Q, loss_a = policy.train(replay_buffer, episode_timesteps, args.batch_size, args.discount, args.tau, args.policy_noise, args.noise_clip, args.policy_freq)
					writer.add_scalar('train/Q_loss', loss_Q, total_timesteps)
					writer.add_scalar('train/actor_loss', loss_a, total_timesteps)

				else:
					policy.train(replay_buffer, episode_timesteps, args.batch_size, args.discount, args.tau)


				writer.add_scalar('train/rewards', episode_reward, total_timesteps)



			# Evaluate episode
			if timesteps_since_eval >= args.eval_freq:
				timesteps_since_eval %= args.eval_freq
				evaluations.append(evaluate_policy(policy))

				writer.add_scalar('eval/rewards', evaluations[-1], total_timesteps)


				if args.save_models:
					policy.save(file_name, directory="./pytorch_models")
					# np.save("./results/%s" % (file_name), evaluations)




			# Reset environment
			obs = env.reset()
			done = False
			episode_reward = 0
			episode_timesteps = 0
			episode_num += 1
		
		# Select action randomly or according to policy
		if total_timesteps < args.start_timesteps:
			action = env.action_space.sample()
			if max_action is None:
				action_np = np.zeros(action_dim)
				action_np[action] = 1
				action = action_np
		else:

			action = policy.select_action(np.array(obs))
			if args.expl_noise != 0:
				if max_action is not None:
					action = (action + np.random.normal(0, args.expl_noise, size=env.action_space.shape[0])).clip(env.action_space.low, env.action_space.high)
				else:
					if np.random.rand() < args.expl_noise:
						action = np.zeros(action_dim)
						action[np.random.randint(0, action_dim)] = 1

		# Perform action
		new_obs, reward, done, _ = env.step(action if max_action is not None else np.argmax(action))
		done_bool = 0 if episode_timesteps + 1 == env._max_episode_steps else float(done)
		episode_reward += reward

		# Store data in replay buffer
		replay_buffer.add((obs, new_obs, action, reward, done_bool))

		obs = new_obs

		episode_timesteps += 1
		total_timesteps += 1
		timesteps_since_eval += 1


		state_action_history.append([np.array(obs), action])


	# Final evaluation 
	evaluations.append(evaluate_policy(policy))
	if args.save_models: policy.save("%s" % (file_name), directory="./pytorch_models")
	# np.save("./results/%s" % (file_name), evaluations)

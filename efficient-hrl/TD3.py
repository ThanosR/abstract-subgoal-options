import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import utils
from torch.distributions import Categorical

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Implementation of Twin Delayed Deep Deterministic Policy Gradients (TD3)
# Paper: https://arxiv.org/abs/1802.09477


class Actor(nn.Module):
	def __init__(self, state_dim, action_dim, max_action = None):
		super(Actor, self).__init__()

		self.l1 = nn.Linear(state_dim, 300)
		self.l2 = nn.Linear(300, 300)
		self.l3 = nn.Linear(300, action_dim)

		if max_action is not None:
			self.max_action = torch.FloatTensor(max_action).to(device)
		else:
			self.max_action = None


	def forward(self, x):
		x = F.relu(self.l1(x))
		x = F.relu(self.l2(x))
		if self.max_action is not None:
			x = self.max_action * torch.tanh(self.l3(x))
		else:
			x = torch.nn.functional.softmax(self.l3(x), dim=1)

		return x


class Critic(nn.Module):
	def __init__(self, state_dim, action_dim):
		super(Critic, self).__init__()

		# Q1 architecture
		self.l1 = nn.Linear(state_dim + action_dim, 400)
		self.l2 = nn.Linear(400, 300)
		self.l3 = nn.Linear(300, 1)

		# Q2 architecture
		self.l4 = nn.Linear(state_dim + action_dim, 400)
		self.l5 = nn.Linear(400, 300)
		self.l6 = nn.Linear(300, 1)


	def forward(self, x, u):
		xu = torch.cat([x, u], 1)

		x1 = F.relu(self.l1(xu))
		x1 = F.relu(self.l2(x1))
		x1 = self.l3(x1)

		x2 = F.relu(self.l4(xu))
		x2 = F.relu(self.l5(x2))
		x2 = self.l6(x2)
		return x1, x2


	def Q1(self, x, u):
		xu = torch.cat([x, u], 1)

		x1 = F.relu(self.l1(xu))
		x1 = F.relu(self.l2(x1))
		x1 = self.l3(x1)
		return x1 


class TD3(object):
	def __init__(self, state_dim, action_dim, max_action, lr_a = None, lr_c = None):
		self.state_dim = state_dim
		self.action_dim = action_dim

		self.actor = Actor(state_dim, action_dim, max_action).to(device)

		if lr_a is not None:
			self.actor_optimizer = torch.optim.Adam(self.actor.parameters(), lr=lr_a)
		else:
			self.actor_optimizer = torch.optim.Adam(self.actor.parameters())

		self.actor_target = Actor(state_dim, action_dim, max_action).to(device)
		self.actor_target.load_state_dict(self.actor.state_dict())
		self.critic = Critic(state_dim, action_dim).to(device)
		self.critic_target = Critic(state_dim, action_dim).to(device)
		self.critic_target.load_state_dict(self.critic.state_dict())

		if lr_c is not None:
			self.critic_optimizer = torch.optim.Adam(self.critic.parameters(), lr=lr_c)
		else:
			self.critic_optimizer = torch.optim.Adam(self.critic.parameters())


		if max_action is not None:
			self.max_action = torch.FloatTensor(max_action).to(device)
		else:
			self.max_action = None


	def select_action(self, state):
		state = torch.FloatTensor(state.reshape(1, -1)).to(device)

		if self.max_action is not None:
			action = self.actor(state)
			return action.cpu().data.numpy().flatten()
		else:
			output = self.actor(state)
			m = Categorical(output)
			action = m.sample()

			action_np = np.zeros(self.action_dim)
			action_np[action[0]] = 1
			return action_np


	def train(self, replay_buffer, iterations, batch_size=100, discount=0.99, tau=0.005, policy_noise=0.2, noise_clip=0.5, policy_freq=2):

		mean_q_loss = 0
		mean_a_loss = 0

		for it in range(iterations):

			# Sample replay buffer 
			x, y, u, r, d = replay_buffer.sample(batch_size)
			state = torch.FloatTensor(x).to(device)
			action = torch.FloatTensor(u).to(device)
			next_state = torch.FloatTensor(y).to(device)
			done = torch.FloatTensor(1 - d).to(device)
			reward = torch.FloatTensor(r).to(device)

			# Select action according to policy and add clipped noise
			if self.max_action is not None:
				noise = torch.FloatTensor(u).data.normal_(0, policy_noise).to(device)
				noise = noise.clamp(-noise_clip, noise_clip)
				next_action = self.actor_target(next_state) + noise * self.max_action
				next_action = torch.max(-self.max_action, next_action)
				next_action = torch.min(next_action, self.max_action)
				# next_action = next_action.clamp(-self.max_action, self.max_action)
			else:
				next_action = self.actor_target(next_state)

				if policy_noise > 0:
					action_dim = next_action.shape[1]
					random_mask = (torch.rand(batch_size, 1) < policy_noise).float().to(device)
					random_action = torch.randint(0, action_dim, (batch_size,)).to(device)
					random_action = torch.eye(action_dim).index_select(0, random_action.type(torch.LongTensor)).to(device)

					# next_action = next_action * (1 - random_mask) + random_action * random_mask
					next_action = next_action * (1 - random_mask)
					next_action += random_action * random_mask

			# Compute the target Q value
			target_Q1, target_Q2 = self.critic_target(next_state, next_action)
			target_Q = torch.min(target_Q1, target_Q2)
			target_Q = reward + (done * discount * target_Q).detach()

			# Get current Q estimates
			current_Q1, current_Q2 = self.critic(state, action)

			# Compute critic loss
			critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(current_Q2, target_Q) 

			mean_q_loss += critic_loss.data.item()

			# Optimize the critic
			self.critic_optimizer.zero_grad()
			critic_loss.backward()
			self.critic_optimizer.step()

			# Delayed policy updates
			if it % policy_freq == 0:

				# Compute actor loss
				actor_loss = -self.critic.Q1(state, self.actor(state)).mean()

				mean_a_loss += actor_loss.data.item()

				# Optimize the actor 
				self.actor_optimizer.zero_grad()
				actor_loss.backward()
				self.actor_optimizer.step()

				# Update the frozen target models
				for param, target_param in zip(self.critic.parameters(), self.critic_target.parameters()):
					target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)

				for param, target_param in zip(self.actor.parameters(), self.actor_target.parameters()):
					target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)

		return mean_q_loss / iterations,  mean_a_loss / (iterations // policy_freq)

	def save(self, filename, directory):
		torch.save(self.actor.state_dict(), '%s/%s_actor.pth' % (directory, filename))
		torch.save(self.critic.state_dict(), '%s/%s_critic.pth' % (directory, filename))


	def load(self, filename, directory):
		self.actor.load_state_dict(torch.load('%s/%s_actor.pth' % (directory, filename)))
		self.critic.load_state_dict(torch.load('%s/%s_critic.pth' % (directory, filename)))









class Actor_2(nn.Module):
	def __init__(self, state_dim, action_space):
		super(Actor_2, self).__init__()

		self.action_space = action_space

		self.l1 = nn.Linear(state_dim, 300)
		self.l2 = nn.Linear(300, 300)
		self.l_outs = self.action_space.initialize_output_layers(self.l2.out_features)

		# self.lol = self.l_outs[0]

		# for i, layer in enumerate(self.l_outs):
		# 	locals()['l3{}'.format(i)] = layer

		for i, layer in enumerate(self.l_outs):
			setattr(self, 'l3{}'.format(i), layer)
			# locals()['l3{}'.format(i)] = layer


	def forward(self, x):
		x = F.relu(self.l1(x))
		x = F.relu(self.l2(x))
		x = self.action_space.forward(x, self.l_outs)

		return x


class TD3_2(object):
	def __init__(self, state_dim, action_space, lr_a = None, lr_c = None):
		self.state_dim = state_dim
		self.action_space = action_space

		self.actor = Actor_2(state_dim, action_space).to(device)
		self.actor_target = Actor_2(state_dim, action_space).to(device)
		self.actor_target.load_state_dict(self.actor.state_dict())

		self.actor_optimizer = torch.optim.Adam(self.actor.parameters()) if lr_a is None else torch.optim.Adam(self.actor.parameters(), lr=lr_a)

		self.critic = Critic(state_dim, self.action_space.dim()).to(device)
		self.critic_target = Critic(state_dim, self.action_space.dim()).to(device)
		self.critic_target.load_state_dict(self.critic.state_dict())

		self.critic_optimizer = torch.optim.Adam(self.critic.parameters()) if lr_c is None else torch.optim.Adam(self.critic.parameters(), lr=lr_c)


	def select_action(self, state):
		state = torch.FloatTensor(state.reshape(1, -1)).to(device)
		action = self.actor(state)
		return self.action_space.select_action(action)



	def train(self, replay_buffer, iterations, batch_size=100, discount=0.99, tau=0.005, policy_noise=0.2, noise_clip=0.5, policy_freq=2):

		mean_q_loss = 0
		mean_a_loss = 0

		for it in range(iterations):

			# Sample replay buffer
			x, y, u, r, d = replay_buffer.sample(batch_size)
			state = torch.FloatTensor(x).to(device)
			action = torch.FloatTensor(u).to(device)
			next_state = torch.FloatTensor(y).to(device)
			done = torch.FloatTensor(1 - d).to(device)
			reward = torch.FloatTensor(r).to(device)

			# Select action according to policy and add clipped noise
			next_action = self.actor_target(next_state)
			next_action = self.action_space.add_noise(next_action)

			# Compute the target Q value
			target_Q1, target_Q2 = self.critic_target(next_state, next_action)
			target_Q = torch.min(target_Q1, target_Q2)
			target_Q = reward + (done * discount * target_Q).detach()

			# Get current Q estimates
			current_Q1, current_Q2 = self.critic(state, action)

			# Compute critic loss
			critic_loss = F.mse_loss(current_Q1, target_Q) + F.mse_loss(current_Q2, target_Q)

			mean_q_loss += critic_loss.data.item()

			# Optimize the critic
			self.critic_optimizer.zero_grad()
			critic_loss.backward()
			self.critic_optimizer.step()

			# Delayed policy updates
			if it % policy_freq == 0:

				# Compute actor loss
				actor_loss = -self.critic.Q1(state, self.actor(state)).mean()

				mean_a_loss += actor_loss.data.item()

				# Optimize the actor
				self.actor_optimizer.zero_grad()
				actor_loss.backward()
				self.actor_optimizer.step()

				# Update the frozen target models
				for param, target_param in zip(self.critic.parameters(), self.critic_target.parameters()):
					target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)

				for param, target_param in zip(self.actor.parameters(), self.actor_target.parameters()):
					target_param.data.copy_(tau * param.data + (1 - tau) * target_param.data)

		return mean_q_loss / iterations,  mean_a_loss / (iterations // policy_freq)

	def save(self, filename, directory):
		torch.save(self.actor.state_dict(), '%s/%s_actor.pth' % (directory, filename))
		torch.save(self.critic.state_dict(), '%s/%s_critic.pth' % (directory, filename))


	def load(self, filename, directory):
		self.actor.load_state_dict(torch.load('%s/%s_actor.pth' % (directory, filename)))
		self.critic.load_state_dict(torch.load('%s/%s_critic.pth' % (directory, filename)))




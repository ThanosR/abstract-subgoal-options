import numpy as np
from environments import DoomGridworld


def reconstruct_path(cameFrom, current):
	total_path = [current]
	while current in cameFrom:
		current = cameFrom[current]
		total_path.append(current)
	return list(reversed(total_path))


def heuristic_cost_estimate(neighbor, goal):
	return np.abs(neighbor[0] - goal[0]) + np.abs(neighbor[1] - goal[1]) + 3


def get_neighbors(node, env):
	neighbors = []

	if env.walkable_position((node[0] + 1, node[1])):
		neighbors.append((node[0] + 1, node[1]))

	if env.walkable_position((node[0] - 1, node[1])):
		neighbors.append((node[0] - 1, node[1]))

	if env.walkable_position((node[0], node[1] + 1)):
		neighbors.append((node[0], node[1] + 1))

	if env.walkable_position((node[0], node[1] - 1)):
		neighbors.append((node[0], node[1] - 1))

	return neighbors


def A_Star(start, goal, env):
	start = tuple(start)
	goal = tuple(goal)

	# The set of nodes already evaluated
	closedSet = set()

	# The set of currently discovered nodes that are not evaluated yet.
	# Initially, only the start node is known.
	openSet = {start}

	# For each node, which node it can most efficiently be reached from.
	# If a node can be reached from many nodes, cameFrom will eventually contain the
	# most efficient previous step.
	cameFrom = {}  #an empty map

	# For each node, the cost of getting from the start node to that node.
	gScore = {} #map with default value of Infinity

	# The cost of going from start to start is zero.
	gScore[start] = 0

	# For each node, the total cost of getting from the start node to the goal
	# by passing by that node. That value is partly known, partly heuristic.
	fScore = {} #map with default value of Infinity

	# For the first node, that value is completely heuristic.
	fScore[start] = heuristic_cost_estimate(start, goal)

	while openSet:
		#current = the node in openSet having the lowest fScore[] value
		_min = 1000
		for node in openSet:
			f = fScore[node] if node in fScore else 1000
			if f < _min:
				current = node
				_min = f

		if current == goal:
			return reconstruct_path(cameFrom, current)

		openSet.remove(current)
		closedSet.add(current)

		for neighbor in get_neighbors(current,env):
			if neighbor in closedSet:
				continue		# Ignore the neighbor which is already evaluated.

			g_neighbor = gScore[neighbor] if neighbor in gScore else 1000

			# The distance from start to a neighbor
			tentative_gScore = gScore[current] + 1

			if neighbor not in openSet:	# Discover a new node
				openSet.add(neighbor)
			else:
				if tentative_gScore >= g_neighbor:
					continue		# This is not a better path.

			# This path is the best until now. Record it!
			cameFrom[neighbor] = current
			gScore[neighbor] = tentative_gScore
			fScore[neighbor] = gScore[neighbor] + heuristic_cost_estimate(neighbor, goal)

#
# def get_action_to_rotation(pos, rot, next_pos):
#
# 	if pos[0] == next_pos[0] and pos[1] == next_pos[1]:
# 		return 6
#
# 	# Right
# 	if next_pos[0] == pos[0] + 1:
# 		target_rot = 0
# 	# Up
# 	if next_pos[1] == pos[1] + 1:
# 		target_rot = 1
# 	# Left
# 	if next_pos[0] == pos[0] - 1:
# 		target_rot = 2
# 	# Down
# 	if next_pos[1] == pos[1] - 1:
# 		target_rot = 3
#
# 	rot_diff = target_rot - rot
#
# 	if rot_diff == 0:
# 		return 0 # move forwards
#
#
# 	dir = -np.sign(rot_diff)
#
# 	if np.abs(rot_diff) == 3:
# 		dir *= -1
#
# 	dir = (dir + 1) / 2 # [-1,1] to [0,1]
#
# 	return 4 + dir # 4 = rotate left



def get_action_to(pos, next_pos, rot, use_rotation):
	if use_rotation:
		return int(get_action_to_rotation(pos,rot, next_pos))
	else:
		return int(get_action_to_movement(pos, next_pos))


def get_action_to_rotation(pos, rot, next_pos):

	if pos[0] == next_pos[0] and pos[1] == next_pos[1]:
		return DoomGridworld.A_NOTHING

	# Right
	if next_pos[0] == pos[0] + 1:
		target_rot = 0
	# Up
	if next_pos[1] == pos[1] + 1:
		target_rot = 1
	# Left
	if next_pos[0] == pos[0] - 1:
		target_rot = 2
	# Down
	if next_pos[1] == pos[1] - 1:
		target_rot = 3

	rot_diff = target_rot - rot

	if rot_diff == 0:
		return DoomGridworld.A_MOVE_FORWARDS # move forwards


	dir = -np.sign(rot_diff)

	if np.abs(rot_diff) == 3:
		dir *= -1

	dir = (dir + 1) / 2 # [-1,1] to [0,1]

	return DoomGridworld.A_ROTATE_LEFT + dir # 4 = rotate left


def get_action_to_movement(pos, next_pos):

	if pos[0] == next_pos[0] and pos[1] == next_pos[1]:
		return DoomGridworld.A_NOTHING

	# Right
	if next_pos[0] == pos[0] + 1:
		return DoomGridworld.A_STRAFE_RIGHT
	# Up
	if next_pos[1] == pos[1] + 1:
		return DoomGridworld.A_MOVE_FORWARDS
	# Left
	if next_pos[0] == pos[0] - 1:
		return DoomGridworld.A_STRAFE_LEFT
	# Down
	if next_pos[1] == pos[1] - 1:
		return DoomGridworld.A_MOVE_BACKWARDS



class GridworldGatherPolicyLow(object):

	def __init__(self, action_dim, env):
		self.env = env
		self.action_dim = action_dim
		self.min_clip = np.zeros(2)
		self.max_clip = np.asarray(self.env.default_grid.shape) - 1

		pass

	def select_action(self, state):

		current_pos = state[:2].astype(int)
		rotation = np.argmax(state[2:6])
		target_pos = state[-2:] + current_pos
		target_pos = target_pos.astype(int)


		target_pos = np.clip(target_pos, self.min_clip, self.max_clip)

		path = A_Star(current_pos, target_pos, self.env)
		if path is None or len(path) == 1:
			next_pos = current_pos
		else:
			next_pos = path[1]

		action = get_action_to(current_pos, next_pos, rotation, self.action_dim > 5)

		action_np = np.zeros(self.action_dim)
		action_np[action] = 1

		return action_np

	def save(self, filename, directory):
		pass


	def train(self, replay_buffer, iterations, batch_size=100, discount=0.99, tau=0.005, policy_noise=0.2, noise_clip=0.5, policy_freq=2):
		return 0,0




class GridworldGatherPolicyHigh(object):

	def __init__(self, env):
		self.env = env
		pass

	def select_action(self, state):

		pickups_left = [pickup for pickup in self.env.pickups if not pickup.was_picked]

		closest_pickup = None
		min_dist = np.inf

		current_pos = state[:2]

		for pickup in pickups_left:
			dist =  np.abs(pickup.pos[0] - current_pos[0]) + np.abs(pickup.pos[1] - current_pos[1])
			if dist < min_dist:
				min_dist = dist
				closest_pickup = pickup

		if closest_pickup is not None:
			goal = closest_pickup.pos - current_pos
		else:
			goal = current_pos - current_pos

		return goal

	def save(self, filename, directory):
		pass


	def train(self, replay_buffer, iterations, batch_size=100, discount=0.99, tau=0.005, policy_noise=0.2, noise_clip=0.5, policy_freq=2):
		return 0,0

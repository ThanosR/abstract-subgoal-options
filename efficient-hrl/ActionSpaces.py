import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import utils
from torch.distributions import Categorical

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class ActionSpace(object):

	def __init__(self):
		self.action_dims = 0

	def initialize_output_layers(self, in_features):
		return []

	def forward(self, x, output_layers):
		return x

	def select_action(self, actor_output):
		return actor_output

	def dim(self):
		return self.action_dims

	def add_noise(self, action):
		pass

	def logp(selfs, a_tc_new, a_tc):
		"""
		Calculates the probability of a trajectory of actions
		:param a_tc_new: the new actions given by the current model for the same input
		:param a_tc:  the actions for which we want to calculate the probability
		:return:
		"""
		pass





class ContinuousActionSpace(ActionSpace):

	def __init__(self, max_action, noise_clip, policy_noise, expl_noise):
		super().__init__()
		self.max_action = max_action

		self.max_action_tensor = torch.FloatTensor(max_action).to(device)

		self.action_dims = self.max_action.shape[0]
		self.noise_clip = noise_clip
		self.policy_noise = policy_noise
		self.expl_noise = expl_noise

	def initialize_output_layers(self, in_features):
		l = nn.Linear(in_features, self.action_dims)
		return [l]

	def forward(self, x, output_layers):
		l_o = output_layers[0]
		return self.max_action_tensor * torch.tanh(l_o(x))

	def select_action(self, actor_output):
		return actor_output.cpu().data.numpy().flatten()

	def add_noise(self, action):
		"""
		Add Gaussian noise
		:param action:
		:return:
		"""

		if isinstance(action, np.ndarray):
			noise = np.random.normal(0, self.expl_noise, size=self.action_dims)
			action = action + self.max_action_tensor * noise
			action = action.clip(-self.max_action_tensor, self.max_action_tensor)
		elif isinstance(action, torch.Tensor):
			noise = action.data.normal_(0, self.policy_noise).to(device)
			noise = noise.clamp(-self.noise_clip, self.noise_clip)
			action = action + noise * self.max_action_tensor
			action = torch.max(-self.max_action_tensor, action)
			action = torch.min(action, self.max_action_tensor)

		return action

	def logp(selfs, a_tc_new, a_tc):
		#  Sum log(p_l(a|s,g)) ~  - 0.5 Sum ||a_tc - mu_p (s,g)||
		dist = a_tc_new - a_tc
		log_p = - 0.5 * np.square(dist).sum()
		return  log_p





class DiscreteActionSpace(ActionSpace):

	def __init__(self, action_dims, policy_noise, noise_clip, expl_noise):
		super().__init__()
		self.action_dims = action_dims
		self.noise_clip = noise_clip
		self.policy_noise = policy_noise
		self.expl_noise = expl_noise



	def initialize_output_layers(self, in_features):
		l = nn.Linear(in_features, self.action_dims)
		return [l]


	def forward(self, x, output_layers):
		l_o = output_layers[0]
		return torch.nn.functional.softmax(l_o(x))

	def select_action(self, actor_output):
		m = Categorical(actor_output)
		action = m.sample()

		action_np = np.zeros(self.action_dims)
		action_np[action[0]] = 1
		return action_np


	def add_noise(self, action):
		"""
		Resamples a categorical action
		:param action:
		:return:
		"""
		if isinstance(action, np.ndarray):
			if np.random.rand() < self.expl_noise:
				next_action = np.zeros(self.action_dims)
				next_action[np.random.randint(0, self.action_dims)] = 1
		elif isinstance(action, torch.Tensor):
			if self.policy_noise > 0:
				action_dim = action.shape[1]
				batch_size = action.shape[0]
				random_mask = (torch.rand(batch_size, 1) < self.policy_noise).float().to(device)
				random_action = torch.randint(0, action_dim, (batch_size,)).to(device)
				random_action = torch.eye(action_dim).index_select(0, random_action.type(torch.LongTensor)).to(device)
				next_action = action * (1 - random_mask)
				next_action += random_action * random_mask

		return next_action

	def logp(selfs, a_tc_new, a_tc):
		log_p = np.log((a_tc_new * a_tc).sum(axis=1) + 1e-5).sum()
		return log_p
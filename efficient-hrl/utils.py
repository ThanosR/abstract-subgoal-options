import numpy as np
import torch
from collections import deque
import gym
import environments
from gym import wrappers

# from environments import create_maze_env
# Code based on: 
# https://github.com/openai/baselines/blob/master/baselines/deepq/replay_buffer.py


def setup_env(args):

	if args.env_name.startswith('Ant'):

		from environments import create_maze_env
		env = EnvWithGoal(create_maze_env.create_maze_env(args.env_name), args.env_name)
		env.base_env.seed(args.seed)

		# qpos + qvel + time + goal_x + goal_y (qpos = 3+4 +8*1, qvel = 3+3 + 8*1)
		# only used to get the dimensionality of the observation
		# max_observation = np.asarray([10, 10, .5, 1, 1, 1, 1, 30, 30, 30, 30, 30, 30, 30, 30] + 14 * [10] + [0, 0, 0])
		# max_observation[7:-3] = np.deg2rad(max_observation[7:-3])
		# observation_space = gym.spaces.Box(low=-max_observation, high=max_observation)

		max_goal = np.asarray([10, 10, .5, 1, 1, 1, 1] + [60, 40, 60, 40, 60, 40, 60, 40] + 14 * [10])
		max_goal[7:] = np.deg2rad(max_goal[7:])
		goal_space = gym.spaces.Box(low=-max_goal, high=max_goal)

		normalizer_low = HistoryRewardNormalizer(1)
		normalizer_high = HistoryRewardNormalizer(0.1)

		observation_dim = 32
		state_to_goal_filter = lambda obs: obs[:-3] if len(obs.shape) == 1 else obs[:, :-3]

	else:

		env = gym.make(args.env_name)
		env.seed(args.seed)

		# env = wrappers.Monitor(env, "./test_monitor",
		#                        video_callable=lambda episode_id: episode_id % int(1) == 0,
		#                        force=True)


		state_to_goal_filter = lambda obs: obs
		normalizer_low, normalizer_high = NoRewardNormalizer(), NoRewardNormalizer()

		if args.env_name == 'DoomGridworldKill-v0':
			# [posX, posY, rotation, health, ammo, ammoX, ammoY] + 8 * [enemy_health]
			# max_goal = np.asarray([10, 10, 4, 100, 5, 10,10] + 8 *[100])
			# max_goal = np.asarray([10, 10, 1, 1, 1, 1, 100, 5, 10,10] + 8 *[100])
			max_goal = np.ones(env.observation_space.shape[0])
			goal_space = gym.spaces.Box(low=-max_goal, high=max_goal)

		if args.env_name == 'DoomGridworldGather-v0':
			state_to_goal_filter = lambda obs: obs[:2] if len(obs.shape) == 1 else obs[:, :2]
			max_goal = args.c * np.ones(2)
			goal_space = gym.spaces.Box(low=-max_goal, high=max_goal)

		if args.env_name == 'GridworldCollect-v0':
			state_to_goal_filter = lambda obs: obs[:2] if len(obs.shape) == 1 else obs[:, :2]
			max_goal = args.c * np.ones(2)
			goal_space = gym.spaces.Box(low=-max_goal, high=max_goal)


			# Full state space
			# state_to_goal_filter = lambda obs: obs



		if args.env_name == 'MountainCarContinuous-v0':
			max_goal = np.asarray([0.3, 0.07])
			goal_space = gym.spaces.Box(low=-max_goal, high=max_goal)
			normalizer_low = HistoryRewardNormalizer(1)
			normalizer_high = HistoryRewardNormalizer(0.1)

		if args.env_name == 'Pendulum-v0' or args.env_name == 'MyDiscretePendulum-v0':
			state_to_goal_filter = lambda obs: obs
			max_goal = np.asarray([1, 1, 8])
			goal_space = gym.spaces.Box(low=-max_goal, high=max_goal)


	return env, goal_space, state_to_goal_filter, normalizer_high, normalizer_low



def h(s_t, g_t,  s_t1, state_to_goal_filter):
	s_t = state_to_goal_filter(s_t)
	s_t1 = state_to_goal_filter(s_t1)
	return s_t + g_t - s_t1



def intrinsic_reward(s_t, g_t, a_t, s_t1, state_to_goal_filter, observation_space = None):
	"""
	Calculate the distance based on the distance between the next state from the goal: D = |G - s_{t+1}|
	:param s_t:
	:param g_t:
	:param a_t:
	:param s_t1:
	:param state_to_goal_filter:
	:param observation_space:
	:return:
	"""
	s_t = state_to_goal_filter(s_t)
	s_t1 = state_to_goal_filter(s_t1)

	g_t = np.rint(g_t)

	absolute_goal = s_t + g_t
	if observation_space is not None:
		absolute_goal = np.clip(absolute_goal, 0, state_to_goal_filter(observation_space.nvec))

	# return -np.linalg.norm(absolute_goal - s_t1, 2)
	return -np.linalg.norm(absolute_goal - s_t1, 1)


def intrinsic_reward2(s_t, g_t, a_t, s_t1, state_to_goal_filter, observation_space = None):
	"""
	Calculate reward based on the difference of the distances between the current state with the goal and the next state with the goal
	D = |G - s_t| - |G - s_{t+1}|
	:param s_t:
	:param g_t:
	:param a_t:
	:param s_t1:
	:param state_to_goal_filter:
	:param observation_space:
	:return:
	"""
	s_t = state_to_goal_filter(s_t)
	s_t1 = state_to_goal_filter(s_t1)

	g_t = np.rint(g_t)

	absolute_goal = s_t + g_t
	if observation_space is not None:
		absolute_goal = np.clip(absolute_goal, 0, state_to_goal_filter(observation_space.nvec))

	return np.linalg.norm(g_t, 2) - np.linalg.norm(absolute_goal - s_t1, 2)





def abstract_intrinsic_reward(s_t, g_m_t, a_t, s_t1, state_to_goal_filter):

	if len(g_m_t.shape) == 1:
		g_idx = int(g_m_t.shape[0] / 2)
		g_t = g_m_t[:g_idx]
		m_t = g_m_t[g_idx:]
	else:
		g_idx = int(g_m_t.shape[1] / 2)
		g_t = g_m_t[:,g_idx]
		m_t = g_m_t[:,g_idx:]

	s_t = state_to_goal_filter(s_t)
	s_t1 = state_to_goal_filter(s_t1)
	return -np.linalg.norm((s_t + g_t - s_t1) * m_t, 2) / np.square(m_t).sum()
# return -np.sum(np.square(s_t + g_t - s_t1)) ** 0.5




def get_goal_sample_fn(env_name):
	if env_name == 'AntMaze':
		# NOTE: When evaluating (i.e. the metrics shown in the paper,
		# we use the commented out goal sampling function.  The uncommented
		# one is only used for training.
		# return lambda: np.array([0., 16.])
		return lambda: np.random.uniform((-4, -4), (20, 20))
	elif env_name == 'AntPush':
		return lambda: np.array([0., 19.])
	elif env_name == 'AntFall':
		return lambda: np.array([0., 27., 4.5])
	else:
		assert False, 'Unknown env'

def get_reward_fn(env_name):
	if env_name == 'AntMaze':
		return lambda obs, goal: -np.sum(np.square(obs[:2] - goal)) ** 0.5
	elif env_name == 'AntPush':
		return lambda obs, goal: -np.sum(np.square(obs[:2] - goal)) ** 0.5
	elif env_name == 'AntFall':
		return lambda obs, goal: -np.sum(np.square(obs[:3] - goal)) ** 0.5
	else:
		assert False, 'Unknown env'

def success_fn(last_reward):
	return last_reward > -5.0


class EnvWithGoal(object):
	def __init__(self, base_env, env_name):
		self.base_env = base_env
		self.goal_sample_fn = get_goal_sample_fn(env_name)
		self.reward_fn = get_reward_fn(env_name)
		self.goal = None

		self.evaluation_phase = False
		self.env_name = env_name
		self._max_episode_steps = 500

	def reset(self, evaluation_phase):
		self.evaluation_phase = evaluation_phase
		obs = self.base_env.reset()
		self.goal = self.goal_sample_fn() if not self.evaluation_phase else np.array([0., 16.])
		return np.concatenate([obs, self.goal])

	def step(self, a):
		obs, _, done, info = self.base_env.step(a)
		reward = self.reward_fn(obs, self.goal)
		if self.env_name == 'AntMaze':
			done = success_fn(reward)

		if not done:
			done = self.base_env.t == self._max_episode_steps

		return np.concatenate([obs, self.goal]), reward, done, info

	@property
	def action_space(self):
		return self.base_env.action_space



class HistoryRewardNormalizer(object):

	def __init__(self, scale):
		self.min = np.inf
		self.max = -np.inf
		self.scale = scale

	def normalize(self, r):

		self.min, self.max = min(self.min, r), max(self.max, r)
		r =  self.scale * (r - self.min) / (self.max - self.min) if (self.max - self.min) != 0 else 0

		return r


class NoRewardNormalizer(object):

	def __init__(selfs):
		pass

	def normalize(self, r):
		return r


class MinMaxRewardNormalizer(object):

	def __init__(self, min, max, scale):
		self.min = min
		self.max = max
		self.scale = scale

	def normalize(self, r):
		return self.scale * (r - self.min) / (self.max - self.min) if (self.max - self.min) != 0 else 0
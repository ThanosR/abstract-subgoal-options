# Copyright 2018 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Random policy on an environment."""

# import tensorflow as tf
import numpy as np
import random
import argparse

from environments import create_maze_env
from utils import EnvWithGoal
from utils import success_fn

def run_environment(env_name, episode_length, num_episodes):
    env = EnvWithGoal(
        create_maze_env.create_maze_env(env_name),
        env_name)

    def action_fn(obs):
        action_space = env.action_space
        action_space_mean = (action_space.low + action_space.high) / 2.0
        action_space_magn = (action_space.high - action_space.low) / 2.0
        random_action = (action_space_mean +
                         action_space_magn *
                         np.random.uniform(low=-1.0, high=1.0,
                                           size=action_space.shape))
        return random_action


    print(env.base_env.action_space.low)
    print(env.base_env.action_space.high)

    print(env.base_env.wrapped_env.observation_space.low)
    print(env.base_env.wrapped_env.observation_space.high)

    rewards = []
    successes = []
    for ep in range(num_episodes):
        rewards.append(0.0)
        successes.append(False)
        obs = env.reset(False)

        for _ in range(episode_length):
            obs, reward, done, _ = env.step(action_fn(obs))
            rewards[-1] += reward
            successes[-1] = success_fn(reward)
            env.base_env.render()
            print(obs)
            if done:
                break
        print('Episode %d reward: %.2f, Success: %d'.format( ep + 1, rewards[-1], successes[-1]))

    print('Average Reward over %d episodes: %.2f'.format(num_episodes, np.mean(rewards)))
    print('Average Success over %d episodes: %.2f',num_episodes, np.mean(successes))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--env", default="AntMaze")
    parser.add_argument("--episode_length", default=500)
    parser.add_argument("--num_episodes", default=50)

    args = parser.parse_args()

    run_environment(args.env, args.episode_length, args.num_episodes)

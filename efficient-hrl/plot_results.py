import numpy as np
import matplotlib.pyplot as plt


results_filename = 'TD3_HalfCheetah-v2_0.npy'

data = np.load('./results/{}'.format(results_filename))

if results_filename.startswith('HIRO'):
	f, (ax1, ax2) = plt.subplots(1, 2)

	ax1.plot(data[:,0])
	ax1.set_title('Rewards')

	ax2.plot(data[:,1])
	ax2.set_title('Intrinsic rewards')

else:
	plt.plot(np.asarray(range(len(data))) * 5000, data)
	plt.title('Rewards')

plt.show()
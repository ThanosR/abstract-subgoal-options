import numpy as np
import torch
from collections import deque
import gym
import environments

# Simple replay buffer
class ReplayBuffer(object):
	def __init__(self, buffer_size = 200000):

		self.buffer_size = buffer_size
		self.count = 0
		self.storage = deque()


	# Expects tuples of (state, next_state, action, reward, done)
	def add(self, data):

		self.storage.append(data)

		if len(self.storage) == self.buffer_size + 1:
			self.storage.popleft()


	def sample(self, batch_size = 100):
		ind = np.random.randint(0, len(self.storage), size=batch_size)
		x, y, u, r, d = [], [], [], [], []

		for i in ind:
			X, Y, U, R, D = self.storage[i]
			x.append(np.array(X, copy=False))
			y.append(np.array(Y, copy=False))
			u.append(np.array(U, copy=False))
			r.append(np.array(R, copy=False))
			d.append(np.array(D, copy=False))

		return np.array(x), np.array(y), np.array(u), np.array(r).reshape(-1, 1), np.array(d).reshape(-1, 1)




class ReplayBufferHigh(object):

	def __init__(self, buffer_size = 200000, policy_low = None, goal_space = None, state_to_goal_filter = None, resamples_goal = True, no_goal_samples = 8):

		self.buffer_size = buffer_size
		self.storage = deque()

		self.policy_low = policy_low
		self.goal_space = goal_space
		self.state_to_goal_filter = state_to_goal_filter

		self.resamples_goal = resamples_goal
		self.no_goal_samples = no_goal_samples

		self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

	def add_high_exp(self, episode_buffer, new_state, done_bool):
		# s_{t:t+c-1}, g_{t:t+c-1}, a_{t:t+c-1}, R_{t:t+c-1}, s_{t+c}, d_{t+c}
		high_exp = list(zip(*episode_buffer)) #s,g,a,R
		high_exp.append(new_state) #add s_c
		high_exp.append(done_bool) #
		self.add(high_exp)

		if (len(high_exp) != 6):
			print("Error, high exp dims:", len(high_exp))
			print(high_exp)

	# Expects tuples of (state, next_state, action, reward, done)
	def add(self, data):

		self.storage.append(data)
		if len(self.storage) == self.buffer_size + 1:
			self.storage.popleft()


	def sample(self, batch_size = 100):

		ind = np.random.randint(0, len(self.storage), size=batch_size)

		s_t, g_t, R_t, s_c, d_c = None, None, None, None, None

		for b in ind:

			exp = self.storage[b]

			s_t_exp, g_t_exp, R_t_exp, s_c_exp, d_c_exp = self.resample_goals(exp, self.no_goal_samples)

			s_t = np.append(s_t, s_t_exp, axis = 0) if s_t is not None else s_t_exp
			g_t = np.append(g_t, g_t_exp, axis = 0) if g_t is not None else g_t_exp
			R_t = np.append(R_t, R_t_exp, axis = 0) if R_t is not None else R_t_exp
			s_c = np.append(s_c, s_c_exp, axis = 0) if s_c is not None else s_c_exp
			d_c = np.append(d_c, d_c_exp, axis = 0) if d_c is not None else d_c_exp

		return s_t, g_t, R_t, s_c, d_c




	def resample_goals(self, high_level_exp, no_goal_samples = 8):
		"""
		Converts a high level experience  s_{t:t+c-1}, g_{t:t+c-1}, a_{t:t+c-1}, R_{t:t+c-1}, s_{t+c}
		to a normal experience batch s_t, g_t, R_t, s_{t+c}, for t E s_{t:t+c-1}
		:param high_level_exp: s_{t:t+c-1}, g_{t:t+c-1}, a_{t:t+c-1}, R_{t:t+c-1}, s_{t+c}
		:param policy_low: The low level policy of the hierarchy
		:param no_goal_samples: Number of goals to sample, in addition to the initial goal and (s_tc - s_t)
		:return:
		"""
		device = self.device

		s_tc, g_tc, a_tc, R_tc, s_c, d_c = high_level_exp
		s_tc = np.asarray(s_tc)
		g_tc = np.asarray(g_tc)
		a_tc = np.asarray(a_tc)
		R_tc = np.asarray(R_tc).reshape((-1,1))


		if self.resamples_goal:
			# print("I AM RESAMPLING FOR THE LOVE OF GOD!!!!")
			# Sample the goals
			#0.5×1/2[high-level action range](and subsequently clipped to lie within the high-level action range
			std = 0.5 * 1/2 * (self.goal_space.high - self.goal_space.low)

			reached_goal = self.state_to_goal_filter(s_c - s_tc[0])
			g_candidates = np.random.normal(reached_goal, std, [no_goal_samples, reached_goal.shape[0]])
			g_candidates = np.append(g_candidates, g_tc[0][None], axis=0)  # original goal
			g_candidates = np.append(g_candidates, reached_goal[None], axis = 0)  # actual state reached
			g_candidates = np.clip(g_candidates, self.goal_space.low, self.goal_space.high)

			# Find the goal with the maximum probability
			max_log_p = -np.inf
			max_g_tc_new = -1

			for i in range(g_candidates.shape[0]):

				# New sequence of goals
				g_tc_new = g_tc - g_tc[0] + g_candidates[i]

				# Extended state-goal space
				sg = np.append(s_tc, g_tc_new, axis = 1)

				# Get probability of each action for the state-goal pair
				state_low = torch.FloatTensor(sg).to(device)
				a_tc_new = self.policy_low.actor(state_low).cpu().data.numpy()

				# Calculate the probability of the trajectory

				if hasattr(self.policy_low, "action_space"):
					log_p = self.policy_low.action_space.logp(a_tc_new, a_tc)
				else:

					if self.policy_low.max_action is not None:
						#  Sum log(p_l(a|s,g)) ~  - 0.5 Sum ||a_tc - mu_p (s,g)||
						dist = a_tc_new - a_tc
						log_p = - 0.5 * np.square(dist).sum()
					else:
						log_p = np.log((a_tc_new * a_tc).sum(axis=1) + 1e-5).sum()


				if(log_p > max_log_p):
					max_log_p = log_p
					max_g_tc_new = g_tc_new
				# print(log_p)

		else:
			max_g_tc_new = g_tc



		R_tc = np.cumsum(R_tc[::-1])[::-1]



		return s_tc, np.tile(s_c, [s_tc.shape[0], 1]), max_g_tc_new, R_tc.reshape((-1,1)), np.tile(d_c, [s_tc.shape[0], 1])



